import codecs

f = codecs.open('Problem13.txt',"r", "utf-8-sig")
lines = f.readlines()
f.close()

print sum( [ long(q) for q in lines])
