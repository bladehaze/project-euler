import MathMo
import math

all = set()
for a in range(2,101):
    for b in range(2,101):
        all.add(math.pow(a,b))

print len(all)
