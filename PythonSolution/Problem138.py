import MathMo
import math
import gmpy2
from gmpy2 import mpz
tt = [x for x in MathMo.FirstK(MathMo.continuedfraction(math.sqrt(7)),10)]
z = MathMo.RationalApproximation(MathMo.continuedfraction(math.sqrt(7)),3)

#z = MathMo.RationalApproximation(MathMo.continuedfractionsqrt((61)),100)
x,y = MathMo.PellEquationFundamental(5)

k = 0

B = 16
L = 17
xp = 5*B -4
yp = 2*L
sums = 17
for x,y in MathMo.PellGeneralSolution(x,y,xp,yp,5):
    k +=1
    if k > 11:
        break
    if y%2 == 0 and (x+4) %5 == 0:
        B = (x+4)/5
        L = y/2
        sums +=L
        print "B","L", B, L
        TT = mpz((4*L**2 - B**2)/4)
        if(gmpy2.is_square(TT)):
            print gmpy2.isqrt(TT)-B
    if y%2 == 0 and (x-4) %5 == 0:
        B = (x-4)/5
        L = y/2
        print "B","L", B, L
        sums +=L
        TT = mpz((4*L**2 - B**2)/4)
        if(gmpy2.is_square(TT)):
            print gmpy2.isqrt(TT) - B
print "Solution", sums
