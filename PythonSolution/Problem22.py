f = open('Problem22names.txt')
lines = f.readlines()
f.close()
def score(z):
    return sum([ord(x)-96 for x in z.lower()])
names = sorted([x.replace('\"','').lower() for x in lines[0].split(',')])
score = [score(z) for z in names]
t = [x+1 for x in range(5163)]
x = sum([x*y for x,y in zip(score,t)])
