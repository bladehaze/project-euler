import re
import gmpy2
import numpy
import math

def Recurrent(func, hist):
    while True:
        p = func(hist);
        hist =  hist[1:] + [p]
        yield p

def todigits(num):
    return [int(x) for x in str(num)]
def tonumber(ds):
    return long(''.join([str(x) for x in ds]))
def isPrime(n):
    # see http://www.noulakaz.net/weblog/2007/03/18/a-regular-expression-to-check-for-prime-numbers/
    return re.match(r'^1?$|^(11+?)\1+$', '1' * n) == None


def allprime(N):
    M = 100              # upper-bound of search space
    l = list()
    for i in range(2,N+1):
        if isPrime(i):
            yield i
import numpy
def primesfrom2to(n):
#""" Input n>=6, Returns a array of primes, 2 <= p < n """
    sieve = numpy.ones(n/3 + (n%6==2), dtype=numpy.bool)
    for i in xrange(1,int(n**0.5)/3+1):
        if sieve[i]:
            k=3*i+1|1
            sieve[       k*k/3     ::2*k] = False
            sieve[k*(k-2*(i&1)+4)/3::2*k] = False
    return numpy.r_[2,3,((3*numpy.nonzero(sieve)[0][1:]+1)|1)]
 
def ispalindromic(m):
    reverse = 0
    k = m
    while(k>0):
        reverse = reverse*10+k%10
        k/=10
    return m == reverse

def digitsum(k):
    reverse = 0
    while(k>0):
        reverse += k%10
        k/=10
    return reverse

def digitprod(k):
    reverse = 0
    while(k>0):
        reverse *= k%10
        k/=10
    return reverse

def digitsum(k,func):
    reverse = 0
    while(k>0):
        reverse += func(k%10)
        k/=10
    return reverse

def continuedfraction(r):
    z = 0
    while z != r:
        z = int(r)
        yield z
        if r != z:
            r = 1/(r-z)
def c_fraction_from_root(n):
    '''
    Construct a continued fraction from a square root. The argument
    `n` should be an integer representing the radicand of the root:

        >>> from_root(2)
        (1, [2])
        >>> from_root(4)
        (2,)
        >>> from_root(97)
        (9, [1, 5, 1, 1, 1, 1, 1, 1, 5, 1, 18])
    '''

    a0 = int(math.floor(math.sqrt(n)))
    r = (a0, [])
    a, b, c = 1, 2 * a0, a0 ** 2 - n
    delta = math.sqrt(4*n)

    while True:
        try:
            d = int((b + delta) / (2 * c))
        except ZeroDivisionError: # a perfect square
            return (r[0],)
        a, b, c = c, -b + 2*c*d, a - b*d + c*d ** 2
        r[1].append(abs(d))
        if abs(a) == 1:
            break
    return r

def RationalApproximation(seq, K):
    if(K==0) :
        return gmpy2.mpq(seq.next(),1)
    return gmpy2.mpq(seq.next(),1) + 1/RationalApproximation(seq, K-1)


def cycleiter(seq):
    le = len(seq)
    p = 0;
    while True:
        yield seq[p%le]
        p+=1

def PellEquationFundamental(n):
    # x**2 - n * y**2 = 1
    K = 0
    sqrtn = c_fraction_from_root(n)
    IPart = gmpy2.mpq(sqrtn[0])
    while True:
        #tt = RationalApproximation(continuedfraction(sqrtn),K)
        tt = IPart + 1/RationalApproximation(cycleiter(sqrtn[1]),K)
        #print tt
        if (tt.numerator **2 - n * tt.denominator**2 == 1):
            return tt.numerator,tt.denominator
        K +=1
def PellGeneralSolution(x0,y0,x1,y1,n):
    # Generate pell solutions from fundamental solution x0,y0 and
    # One solution x1,y1
    while True:
        xx = x0*x1 + n*y0*y1
        yy = x0*y1 + y0*x1
        yield xx,yy
        x1 = xx
        y1 = yy

def FirstK(seq,K):
    i = 0
    while i < K:
        yield seq.next()
        i +=1
def farey(limit):

    '''Fast computation of Farey sequence as a generator'''

    # n, d is the start fraction n/d (0,1) initially                            

    # N, D is the stop fraction N/D (1,1) initially                             

    pend = []
    n = 0
    d = N = D = 1
    while True:
        mediant_d = d + D
        if mediant_d <= limit:
            mediant_n = n + N
            pend.append((mediant_n, mediant_d, N, D))
            N = mediant_n
            D = mediant_d
        else:
            yield n, d
            if pend:
                n, d, N, D = pend.pop()
            else:
                break
def relatively_prime_generator(n, a=1, b=1):

    ### generates all relatively prime pairs <= n.  The larger number comes first.

    yield (a,b)
    k = 1
    while a*k+b <= n:
        for i in relatively_prime_generator(n, a*k+b, a):
            yield i
        k += 1
        
def divisors(number, testprime):
    for x in testprime:
        if number % x == 0:
            y = x;
            count = 0
            while number % y ==0:
                count +=1
                number /=y
            yield x
    if number != 1 :
        yield number

def phi(number, testprime):
    p = 1
    for x in testprime:
        if number % x == 0:
            y = x;
            count = 0
            while number % y ==0:
                count +=1
                if count == 1:
                    p *= y-1
                else:
                    p*=y
                number /=y
    if(number !=1):
        p *= number-1
    return p

def mobius(n):
    result, i = 1, 2
    while n >= i:
        if n % i == 0:
            n = n / i
            if n % i == 0:
                return 0
            result = -result
        i = i + 1
    return result
def mobiuswithprimes(n, primes):
    sq = math.sqrt(n)+1
    x = numpy.ones(n,dtype=numpy.int)
    for p in primes:
        q = p
        s = q
        x[s-1::p] = x[s-1::p]*(-1)
        if p > sq:
            continue
        q *= p
        while q <= n:
            x[q-1::q] = 0
            if q > sq:
                break
            q *=p
    return x

def sumphiwithknownprime(n, primes):
    mmd = mobiuswithprimes(n,primes)
    print "done with mobius"
    p = 1
    for z in xrange(1,n+1):
        p +=mmd[z-1]*int(n/z)**2
        if z %50000 == 0:
            print z
    return p/2

    
def sumphi(n):
    p = 1
    for z in xrange(1,n+1):
        p +=mobius(z)*int(n/z)**2
        if z %50000 == 0:
            print z
    return p/2

