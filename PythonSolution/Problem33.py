import MathMo
import gmpy2
import numpy
from gmpy2 import mpz
dens = []
nums = []
for co in range(0,10):
    for nu in range(0,10):
        for deno in range(0,10):
            if(nu==deno): continue
            if(co==0 or (nu==0 or deno==0 )):continue
            if(co==0):
                nnu = [[nu,co]]
            else:
                nnu = [[nu,co],[co,nu]]

            if(co==0):
                dnn = [[deno,co]]
            else:
                dnn = [[deno,co],[co,deno]]
            for nnz in nnu:
                for dnnz in dnn:
                    nnn = MathMo.tonumber(nnz)
                    ddd = MathMo.tonumber(dnnz)
                    #print nnn,ddd
                    if(nnn>=ddd): continue
                    if(nnn * deno ==  ddd* nu):
                        print nnn,ddd,nu,deno
                        dens.append(deno)
                        nums.append(nu)
Nu = mpz(int(numpy.product(nums)))
Dn = mpz(int(numpy.product(dens)))
gcc = gmpy2.gcd(Nu,Dn)
print "result" , Dn/gcc


