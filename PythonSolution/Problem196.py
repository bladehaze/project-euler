import gmpy2
#import MathMo
#allprm = set(MathMo.primesfrom2to(7208785*(7208785+1)/2))

def releventset(rown,i):
    a = set([-1,1,-(rown-1),-(rown-1)-1,-(rown-1)+1,(rown)-1,(rown),(rown)+1])
    if i ==0:
        a = a.difference( [-1,-(rown-1)-1,(rown)-1])
    if i == rown-1:
        a = a.difference( [-(rown-1),-(rown-1)+1,1])
    if i == rown-2:
        a = a.difference( [-(rown-1)+1])
    return a
def IndexRelative(thisrow, thisp, newp):
    if(newp==thisp+1):
        return thisrow,1 #row count index offset.
    if(newp == thisp-1):
        return thisrow, -1
    if(newp > thisp):
        return thisrow+1, newp-thisp-thisrow
    if(newp<thisp):
        return thisrow-1, newp - thisp+(thisrow-1)
isprime = gmpy2.is_prime
#isprime = lambda x : x in allprm
def SFunction(N):
    First = N*(N-1)/2+1
    sums = 0
    knownp = set()
    for i in xrange(0,N):
        thisp = First+i
        if not isprime(thisp): continue
        #print [x+thisp for x in releventset(N,i)]
        primes = filter(lambda x: x in knownp or isprime(x), [x+thisp for x in releventset(N,i)])
        #knownp = knownp.union(primes)
        if(len(primes)>=2):
            #print thisp
            sums += thisp
        else:
            for newp in primes:
                a = set([newp,thisp])
                newrow,indoff = IndexRelative(N,thisp,newp)
                s = set(filter(lambda x: x in knownp or isprime(x), [x+newp for x in releventset(newrow,i+indoff)]))
                #knownp =knownp.union(s)
                if len(s.difference(a)) > 0:
                    #print thisp
                    sums += thisp
                    break
    return sums
    
print SFunction(5678027)+SFunction(7208785)
