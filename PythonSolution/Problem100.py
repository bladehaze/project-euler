import gmpy2
import math
from gmpy2 import mpz
p0 = mpz(1000000000000)
gn= mpz(1000000000000)
gn= mpz(30)
stop = False
last = 85.0
while not stop:
    if(gmpy2.is_square(gn*(gn-1)*2+1)):
        z = gmpy2.isqrt(gn*(gn-1)*2+1)
        if(z%2==1):
            print (z+1)/2 , gn*1.0/last, gn
            if gn>p0:
                break
            gn = mpz(math.floor(gn *5.82842))
            last = gn

            #stop = True
    gn = gn+1
