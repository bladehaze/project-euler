import gmpy2
import math
from gmpy2 import mpz

def todigits(num):
    return [int(x) for x in str(num)]


stop = False
i = 2749
while not stop:
    tt = gmpy2.fib(i)
    lds = todigits(tt%1000000000)
    p = set(lds[0:9])
    if(0 not in p and len(p)==9):
        print p
        lds = todigits(tt)
        q = set(lds[0:9])
        print q
        if(0 not in q and len(q)==9 ):
            print i
            break
    i = i + 1
