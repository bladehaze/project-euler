import numpy
import MathMo
from itertools import groupby
import gmpy2
import math
from gmpy2 import mpz
row = 20
DPTable = numpy.zeros((row-2)*100).reshape(row-2,100)

def has3digit(n):
    z = MathMo.todigits(n)
    return len(z)==3 and sum(z)<=9

def last2(n):
    z = MathMo.todigits(n)
    return z[1], z[2]

z =[(k,(list(g))) for k,g in groupby(sorted(filter(has3digit, [x for x in range(1001)]), key=last2),key=lambda z:last2(z)) ]

for x, y in z:
    c = len(y)
    ind = MathMo.tonumber(x)
    DPTable[0,ind] = c

def todigits2(n):
    tmp = MathMo.todigits(n)
    if(len(tmp)==1):
        return [0]+tmp
    else:
        return tmp
tt = [(x,todigits2(x)) for x in range(100)]

func = lambda (x,y):x


def myfilter(seq, thisdig):
    for tt in seq:
        y = tt[1]
        if( y[1]==thisdig[0] and sum(y)+thisdig[1]<=9):
            yield tt


for rowind in range(1,row-2):
    for colind in range(100):
        thisdig = todigits2(colind)
        lastind = [x for x in myfilter([(x,todigits2(x)) for x in range(100)],thisdig)]
        DPTable[rowind,colind] = sum([DPTable[rowind-1,i] for i,z in lastind])

print mpz( sum([DPTable[row-3,x] for x in range(100)]))
