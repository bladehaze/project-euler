import MathMo
import gmpy2
qq = MathMo.c_fraction_from_root(2)
IPart = gmpy2.mpq(qq[0])
##sums = 0
##for K in xrange(1000):
##    tt = IPart + 1/MathMo.RationalApproximation(MathMo.cycleiter(qq[1]),K)
##    if len(MathMo.todigits( tt.numerator)) > len(MathMo.todigits( tt.denominator)):
##        sums+=1
##print sums
##

fracpart = [2]*1000

def funcx(n):
    i = 0
    x = gmpy2.mpq(2,1)
    while i < n :
        i+=1
        x = 2 + 1/x
    x = 1/x + 1
    return x
sums = 0
for k in xrange(1000):
    tt = funcx(k)
    #print tt
    if len(MathMo.todigits( tt.numerator)) > len(MathMo.todigits( tt.denominator)):
        sums +=1
print sums
