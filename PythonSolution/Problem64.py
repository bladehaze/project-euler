import MathMo
import gmpy2
odds = 0
for x in xrange(1,10001):
    if(gmpy2.is_square(x)): continue
    p = MathMo.c_fraction_from_root(x)
    if len(p[1])%2==1:
        odds +=1

print odds
