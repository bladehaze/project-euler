import MathMo
import gmpy2
mmax = 0
mmaxind = 0
for i in xrange(1,2001):
    s = 0
    if( gmpy2.is_square(i)):
        s = gmpy2.isqrt(i)
    else:
        s,t = MathMo.PellEquationFundamental(i)
    if s > mmax :
        mmaxind = i
        print mmax
    mmax = max(mmax,s)

print mmaxind
