N = 10000000
def todigits(num):
    return [int(x) for x in str(num)]
def tonumber(ds):
    return long(''.join([str(x) for x in ds]))
def digitsqr(num):
    su = 0
    while num !=0:
        t = num%10
        num = num/10
        if t == 0:
            continue
        su = su + t*t
    return su
def remove0(num):
    su = 0
    while num !=0:
        t = num%10
        num = num/10
        if t == 0:
            continue
        su = su*10 + t
    return su
def all_perms(elements):
    if len(elements) <=1:
        yield elements
    else:
        for perm in all_perms(elements[1:]):
            for i in range(len(elements)):
                #nb elements[0:1] works in both string and list contexts
                yield perm[:i] + elements[0:1] + perm[i:]
def GetAugmentedSet(se):
    for x in se:
        ds = todigits(x)
        for y in all_perms(ds):
            z = [p for p in y]
            zzz = tonumber(z)
            if zzz <= N:
                yield zzz
            
            
                
        
            

def numofzeros(n):
    if(10*n>N):
        return 0
    c = 0
    while n<N:
        n = 10 * n
        c = c + 1
    return c

SetOne = set()
SetOther = set()
tmp = set()
ll = 10000
for i in range(1,N+1):
    if( i in SetOne or i in SetOther):
        continue
    tmp.clear()
    tmp.add(i)
    p = i
    while(p != 1 and p != 89 and p not in SetOne and p not in SetOther):
         p = digitsqr(p)
         tmp.add(p)
    if(p==1 or p in SetOne):
        SetOne = SetOne.union(GetAugmentedSet(tmp))
    else:
        SetOther = SetOther.union(GetAugmentedSet(tmp))
    if i > ll :
        ll = i + 10000
        print i,len(SetOther)

print len(SetOther)
