N = 1000000000
def todigits(num):
    return [int(x) for x in str(num)]
def tonumber(ds):
    return long(''.join([str(x) for x in ds]))
def filter10s(seq):
   for el in seq:
       if el%10!=0:
           yield el
def filterevens(seq):
   for el in seq:
       dd = todigits(el)
       if(dd[-1]>=dd[0] and (dd[0] + dd[-1]) %2 !=0):
           yield dd

def filterall(seq):
   for dd in seq:
       tzip = zip(dd[::-1],dd)
       ag = 0
       for x,y in tzip:
           z = x+y+ag
           if(z % 2 == 0):
               break
           else:
               ag = z/10
       else:
           yield tonumber(dd)
                
def inverseadd(n):
    sums = 0
    m = n
    while(m!=0):
        t = m %10
        sums = sums*10 + t
        m = m/10
    return sums+n
def alldigitodd(n):
    while n != 0:
        t = n%10
        if (t%2==0):
            return False
        n = n/10
    return True
def filters(seq):
    for el in seq:
        if(alldigitodd(inverseadd(el))):
            yield el
           
       
def OneBillion():
    i = 1
    while(i<=N):
        yield i
        i = i + 1
# alln1 =[x for x in filterall(filterevens((filter10s(OneBillion()))))]
alln =[x for x in filters(filter10s(OneBillion()))]
print len(alln)
