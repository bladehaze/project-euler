N = 1000000

def todigits(num):
    return [int(x) for x in str(num)]
def tonumber(ds):
    return long(''.join([str(x) for x in ds]))

def IsplindromeDecimal(n):
    d = todigits(n)
    return d==d[::-1]

def Isplindromebinary(n):
    e = str(bin(n))
    e = e[2:]
    return e==e[::-1]

def filters(seq):
    for m in seq:
        if Isplindromebinary(m) and IsplindromeDecimal(m):
            yield m

def OneM():
    i = 1
    while i<1000000:
        yield i
        i = i + 1

print sum([x for x in filters(OneM())])
