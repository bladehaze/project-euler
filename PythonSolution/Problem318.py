import math
N = 2011
D = 2011 * math.log(10)
def AllPair(Max):
    for q in range(Max-1):
        for p in range(q):
            if(p+q+2<=Max):
                yield p+1,q+1
        
def filter(seq):
    for p,q in seq:
        r = p+q - 2*math.sqrt(p*q)
        if(r < 1):
            yield math.log(r)

print sum([math.ceil(-D/r) for r in filter(AllPair(N))])



