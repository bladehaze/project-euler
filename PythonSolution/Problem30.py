import MathMo
n=1
p = 5
while n * 9**p > 10**(n-1):
    n+=1
res = []
N = n*9**p
for i in xrange(2,N):
    t = MathMo.digitsum(i,lambda x:x**p)
    if(i==t):
        res.append(i)

print res
print sum(res)
