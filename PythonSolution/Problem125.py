import MathMo
import math
import gmpy2
from gmpy2 import mpz
plist = []
N = 100000000
def ispalindromic(m):
    reverse = 0
    k = m
    while(k>0):
        reverse = reverse*10+k%10
        k/=10
    return m == reverse

def allpalindromicnumber(m):
    for i in range(m):
        if(ispalindromic(i+1)):
            yield i+1

def sqrtsum(n):
    return n*(n+1)*(2*n+1)/6

def firstsqrtsumgreaterthan(m):
    if sqrtsum(1)==m: return 1
    start = 1
    end = 2
    z = 1;
    while sqrtsum(end) < m:
        end *=2
        z +=1
    #print end
    # solution between 2^(z-1) 2^z
    left = 2**(z-1)
    right = end
    count = 0;
    while True:
        count +=1
        #print left, right
        mid = (left+right)/2
        if(mid==left):return left
        test = sqrtsum(mid)
        if(test<m):
            left = mid
        if(test>m):
            right = mid
        if(test==m):
            return mid
        if(right-left<=3):
            #print right
            #print count
            return left
        

def issqrtsum(m):
    if(m==1): return False
    KK = int(math.sqrt(m))+1;
    for i in range(0,KK):
        sqri = sqrtsum(i)
        p = firstsqrtsumgreaterthan(m+sqri)
        p = max(i+1,p)
        for j in range(p,KK):
            TT = sqrtsum(j) - sqri
            if(TT==m and j-i>1):
                print i,j,m
                return True
            if(TT>m):
                break
    return False
print "Generate"
#kk = set([x for x in allpalindromicnumber(N)])
print "start"
eset = set();
sums = mpz(0)
t = 0
stop = False
for sumlength in xrange(2,N):
    if(sumlength>N):
        break
    #print sumlength
    
    for j in xrange(N-sumlength):
        t = sqrtsum(j+sumlength) - sqrtsum(j)
        if(t>N ):
            if(j==0): stop=True
            break
        if ispalindromic(t) and t not in eset:
            sums += t
            eset.add(t)
            #print sums
    if(stop):break
    if(sumlength%100000 == 0):
        print sumlength, sums


#l = (filter(issqrtsum, [x for x in allpalindromicnumber(1000)]))
print sums
    
