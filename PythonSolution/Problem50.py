import itertools
def is_prime(x):
    for n in range(2, int(x**.5)+1):
        if x%n == 0:
            return False
    return True
p = [2]
while sum(p) < 1e6:
    p.append(itertools.dropwhile(lambda x:not is_prime(x), (x for x in itertools.count(p[-1]+1))).next())

L = len(p) - 1

for x in range(L, -1, -1):
    for i in range(0, L-x+1):
        if is_prime(sum(p[i:x+i])):
            print sum(p[i:x+i])
            quit()

#Not sure if this is a solution, it gives the right answer but what if the new chain overlaps the old one with long chain,
#This might not happen int this problem because the first couple of prime is very small, however, I dont see this is true
#without some kind of proof.
