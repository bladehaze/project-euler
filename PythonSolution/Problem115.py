import itertools
import MathMo
N = 50




hist2 = [1,2]
hist3 = [1,1,2]
hist4 = [1,1,1,2]
hist = [hist2,hist3,hist4]
func = lambda x : x[0] + x[-1]
sums = 0;
for init in hist:
    t =[x for x in itertools.islice(MathMo.Recurrent(func, init),N-len(init))]
    sums += t[-1]-1
print sums


# Problem 117 slight change
ints = [1,2,4,8]
def Recfunc(init):
    return sum(init)

l = [x for x in itertools.islice(MathMo.Recurrent(Recfunc, ints),N-len(ints))]
print l[-1]
