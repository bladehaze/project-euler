// SolutionC.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <time.h>
#include <iostream>
#include "Problem76.h"
using namespace std;
int _tmain(int argc, _TCHAR* argv[])
{
	clock_t t;
	unsigned int start = clock();
	//Problem14Solution();

	//Problem31Solution();

	//Problem255Solution();
	Problem76Solve(100);
	std::cout << "Time taken in millisecs: " << clock()-start;
	return 0;
}

