#include "stdafx.h"
#include <iostream>
using namespace std;
int Problem76Solve(int N){
	int M = N/2;
	int DPTable[500][500] = {0};
	for(int i = 0 ; i<N ; ++i){
		for(int j = 0; j<M; ++j){
			DPTable[i][j]=0;
		}
	}
	//for(int i = 0 ; i<N ; ++i){
	//		DPTable[i][0]=1;
	//}


	DPTable[0][0]=1;
	DPTable[1][0]=1;
	DPTable[1][1]=0;
	for(int i = 2 ; i<N ; ++i){
		for(int j = 0; j<(i+1)/2; ++j){
			int x = i-j-1;
			int y = j;
			for(int k = y ; k< M ; k++)
				DPTable[i][j] += DPTable[x][k];
			DPTable[i][j] += 1;
		}
	}
	int sum = 0;
	for(int i = 0 ; i<M ; ++i){
		sum += DPTable[N-1][i];
	}
	std::cout << sum << std::endl;
	return sum;

}