#include "stdafx.h"
#include <iostream>
void Problem31Solution(){
	long table[201];
	int coins[8] = {1,2,5,10,20,50,100,200};
	table[0] = 1;
	for(int i = 1;i<=201;++i){
		long ways = 0;
		for(int j = 0;j<8;++j){
			if(coins[j]>i) break;
			ways += table[i-coins[j]];
		}
		table[i] = ways;
	}
	std::cout << table[200] << std::endl;
}