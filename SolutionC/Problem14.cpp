#include "stdafx.h"
#include "Problem14.h"
#include <stack>
#include <iostream>
using namespace std;
std::vector<long long > Problem14(long long  size){
	std::vector<long long > myvector ;
	for(long long  i = 0 ; i<size ;++i){
		myvector.push_back( 0);
	}
	std::stack<long long > mystack ;
	for(long long  i = 113383 ; i<=size ;++i){
		if(myvector[i-1]!=0) continue;
		long long  step = 1;
		long long  st = i;
		mystack.push(st);
		while(st!=1 && myvector[i-1]==0){
			if(st%2==0)
				st /= 2;
			else
				st = 3*st + 1;
			if(st<=size&& myvector[st-1]!=0)
			{
				step = myvector[st-1]+1;
				break;
			}
			step++;
			mystack.push(st);
		}
		while (!mystack.empty()){
			long long  top = mystack.top();
			mystack.pop();
			if(top>size){
				step++;
				continue;
			}
			myvector[top-1] = step;
			step++;
			
		}
	}
	return myvector;
}

void Problem14Solution(){
	std::vector<long long> res = Problem14(1000000);
	long long  res0 = 0;
	long long  max = 0;
	for(long long  i = 0; i<res.size() ; i++)
	{
		if(res[i]>max){
			res0 = i+1;
			max = res[i];
		}
	}
	std::cout << "Max:" << max << std::endl;
	std::cout << "Solution:" << res0 << std::endl;
}