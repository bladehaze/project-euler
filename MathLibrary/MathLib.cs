﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MathLibrary
{
    public static class MathLib
    {
        public static long CountPossibility(double[] logp, double max)
        {
            var counts = logp.Select(e => (int) (max/e)).ToArray();
            return CountPossibility(logp, counts, max, 1);
        }

        public static IEnumerable<int> BreakDigits(int x)
        {
            while (x!=0)
            {
                var t = x%10;
                x /= 10;
                yield return t;
            }
        }
        public static int BreakDigits(IEnumerable<int> inputs)
        {
            int sum = 0;
            foreach (var source in inputs.Reverse())
            {
                sum = 10*sum + source;
            }
            return sum;
        } 




        // for a given block count the ways of n1*logp(1) + ... <=max
        public static long CountPossibility(double[] logp, int[] count, double max, int n)
        {
            if (logp.Length == n)
            {
                var res = max / logp[n - 1] + 1;
                return (long)res;
            }
            long sum = 0;
            var newmax = max;
            for (int i = 0; i <= count[n - 1]; i++)
            {
                if (newmax < 0)
                    break;
                var tmpc = CountPossibility(logp, count, newmax, n + 1);
                sum += tmpc;
                newmax -= logp[n - 1];
            }
            return sum;
        }
        public static IEnumerable<long> Allprime()
        {
            return AllNumber().Where(Isprime);
        } 
        public static IEnumerable<long> AllNumber()
        {
            long z0 = 1;
            while (true)
            {
                yield return z0;
                z0++;
            }
        } 
        public static bool Isprime(long num)
        {
            if (num == 1) return false;
            for (int i = 2; i <= Math.Sqrt(num); i++)
            {
                if (num % i == 0) return false;
            }
            return true;
        }
        public static bool IsNotprime(long num, HashSet<long> knownprime )
        {
            return knownprime.Any(e => num%e == 0) && !knownprime.Contains(num);
        }
        private static bool Isprime(long num, List<long> knownprime, long maxknown )
        {
            var sqrtn = Math.Sqrt(num);
            if (sqrtn < maxknown)
                return knownprime.TakeWhile(e=>e<=sqrtn).All(e => num%e != 0);
            else
            {
                if (knownprime.Any(e => num % e == 0))
                    return false;
                for (long i = maxknown+1; i <= sqrtn; i++)
                {
                    if (num % i == 0) return false;
                }
                return true;
            }
        }
        
        public static long[] AllPrime0(int max)
        {
            var res = Enumerable.Range(1, max).Select(g => (long) g).ToArray();
            res[0] = 0;
            for (int i = 0; i < max; i++)
            {
                if(res[i]==0)continue;
                if(Isprime(res[i]))
                {
                    int index = (int) res[i]*2 - 1;
                    while (index<max)
                    {
                        res[index] = 0;
                        index += (int)res[i];
                    }
                }
            }
            return res;
        }

        public static List<int> FindPrimesBySieveOfAtkins(int max)
        {
            //  var isPrime = new BitArray((int)max+1, false); 
            //  Can't use BitArray because of threading issues.
            var isPrime = new bool[max + 1];
            var sqrt = (int)Math.Sqrt(max);

            Parallel.For(1, sqrt, x =>
            {
                var xx = x * x;
                for (int y = 1; y <= sqrt; y++)
                {
                    var yy = y * y;
                    var n = 4 * xx + yy;
                    if (n <= max && (n % 12 == 1 || n % 12 == 5))
                        isPrime[n] ^= true;

                    n = 3 * xx + yy;
                    if (n <= max && n % 12 == 7)
                        isPrime[n] ^= true;

                    n = 3 * xx - yy;
                    if (x > y && n <= max && n % 12 == 11)
                        isPrime[n] ^= true;
                }
            });

            var primes = new List<int>() { 2, 3 };
            for (int n = 5; n <= sqrt; n++)
            {
                if (isPrime[n])
                {
                    primes.Add(n);
                    int nn = n * n;
                    for (int k = nn; k <= max; k += nn)
                        isPrime[k] = false;
                }
            }

            for (int n = sqrt + 1; n <= max; n++)
                if (isPrime[n])
                    primes.Add(n);

            return primes;
        }

        public static IEnumerable<int> AllPrime(int max)
        {
            var res = Enumerable.Range(1, max).Select(g => g).ToArray();
            res[0] = 0;
            for (int i = 0; i < max; i++)
            {
                if (res[i] < 2) continue;
                if (Isprime(res[i]))
                {
                    int index = (int)res[i] * 2 - 1;
                    while (index < max)
                    {
                        res[index] = 0;
                        index += (int)res[i];
                    }
                    yield return res[i];
                }
            }
        }
        public static BigInteger Modpow(BigInteger basex, BigInteger exponent, BigInteger modulus)
        {

            BigInteger result = 1;

            while (exponent > 0)
            {
                if ((exponent & 1) == 1)
                {
                    // multiply in this bit's contribution while using modulus to keep result small
                    result = (result * basex) % modulus;
                }
                // move to the next bit of the exponent, square (and mod) the base accordingly
                exponent >>= 1;
                basex = ((basex % modulus) * (basex % modulus)) % modulus;
            }

            return result;
        }
        public static Dictionary<int,int> Factor(int n, List<int> primes )
        {
            var fact = new Dictionary<int, int>();
            foreach (var i in primes)
            {
                if (n % i != 0) continue;
                while (n%i==0)
                {
                    n = n/i;
                    if (fact.ContainsKey(i))
                        fact[i] += 1;
                    else
                    {
                        fact.Add(i,1);
                    }
                }
            }
            return fact;
        }
        public static int NumberofFactor(Dictionary<int,int> fact )
        {
            return fact.Values.Select(g=>g+1).Aggregate((cur, i) => cur*(i ));
        }

        public static long Modpow(long basex, long exponent, long modulus)
        {

            long result = 1;

            while (exponent > 0)
            {
                if ((exponent & 1) == 1)
                {
                    // multiply in this bit's contribution while using modulus to keep result small
                    result = (result*basex)%modulus;
                }
                // move to the next bit of the exponent, square (and mod) the base accordingly
                exponent >>= 1;
                basex = ((basex % modulus) * (basex % modulus)) % modulus;
            }

            return result;
        }
        public static BigInteger Choose(BigInteger n, BigInteger k)
        {
            if (k < 0 || k > n)
                return 0;
            if (k > n - k)
                k = n - k;
            BigInteger c = 1;
            for (BigInteger i = 1; i <= k; i++)
            {
                c = c * (n - (k - i));
                c /= i;
            }
            return c;
        }
        public static long Choose(long n, long k)
        {// n choose k
            if (k < 0 || k > n)
                return 0;
            if (k > n - k)
                k = n - k;
            long c = 1;
            for (int i = 1; i <= k; i++)
            {
                c = c*(n - (k - i));
                c /= i;
            }
            return c;
        }

    }
}
