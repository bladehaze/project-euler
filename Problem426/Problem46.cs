﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem46
    {
        private static int N = 1000000;
        static void Main()
        {
            var allprimes = MathLibrary.MathLib.FindPrimesBySieveOfAtkins(N);
            var hash = new HashSet<int>(allprimes);
            var allsquare = Enumerable.Range(1, (int)Math.Sqrt(N) ).Select(g => g*g*2).TakeWhile(e => e <= N).ToList();
            foreach (var allprime in Enumerable.Range(1,N).Select(e=>e*2+1).Where(f=>f<=N&& !hash.Contains(f)))
            {
                if(allsquare.Any(e=>hash.Contains(allprime-e)))
                {
                    continue;
                }
                Console.WriteLine("Solution " + allprime);
                break;
            }
        }
    }
}
