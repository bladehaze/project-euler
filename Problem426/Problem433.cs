﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem433
    {
        private static long res = 0;
        private const int N = 5000000;
        private static Dictionary<Tuple<long, long>, long> dictionary = new Dictionary<Tuple<long, long>, long>();
        private static long[] Counts;
        static void Main()
        {
            Counts = new long[N];
            long z = 0;
            for (long i = 0; i < 31*N; i++)
            {
                z += 2;
            }

            Counts[0] = 0;

            var allf = Fibonaci(2).TakeWhile(e => e <= N).ToList();
            var allprims = MathLibrary.MathLib.FindPrimesBySieveOfAtkins((int) N);
            //Solve2();
            // res = 0;
            // This solve3 is wrong.. just from how many cycles in total to index i, say M, 2*M+1 is not total number of euclid steps, 
            // because it could be one path of M length to i or 2 M/2 path to i
            // and first case, the total number is 2*M+1 second case is 2*M+2.

            // Fixed, add cycle*2 +1 instead of cycle..
            Solve3();
            res += N;
            Console.WriteLine("Result " + res);
        }

        static void Solve3()
        {
            int cycle = 1;
            for (int i = 2; i <=N; i++)
            {
                Counts[i - 1] += 2*cycle+1;
                Mark(cycle+1,1,i);
                if (i<20 || i%50000==0)
                    Console.WriteLine(i);
            }
            for (int i = 1; i < N; i++)
            {
                res += Counts[i]*(N/(i+1));
            }
        }
        static void Mark(int cycle, long x0, long x1)
        {
            long lim = (N-x0)/x1;
            for (int i = 0; i < lim; i++)
            {
                long indx = x0 + (i+1)*x1;
                Counts[indx - 1] += 2*cycle+1;
                Mark(cycle+1,x1,indx);
            }
        }



        static void EvaluateDict(List<KeyValuePair<int,int>> dict,int curentstep )
        {
            //foreach (var VARIABLE in dict)
            //{
                res += dict.Sum(e => N/e.Value)*(2*curentstep+1);
            //}
        }
        static IEnumerable<Tuple<int, int>> GenerateCoprimeOddB1(int m, int n, int max)
        {
            while (true)
            {
                yield return new Tuple<int, int>(m, n);
                var z = m;
                if(max+n<2*m) yield break;
                m = 2*m - n;
                n = z;
            }
        }
        static IEnumerable<Tuple<int, int>> GenerateCoprimeOddB2(int m, int n, int max)
        {
            while (true)
            {
                yield return new Tuple<int, int>(m, n);
                var z = m;
                if(max-n<2*m) yield break;
                m = 2 * m + n;
                n = z;
            }
        }
        static IEnumerable<Tuple<int, int>> GenerateCoprimeOddB3(int m, int n, int max)
        {
            while (true)
            {
                yield return new Tuple<int, int>(m, n);
                if(max-m<2*n) yield break;
                m = m + 2*n;
            }
        }


        static void Solve2()
        {
            for (int i = 2; i <= N; i++)
            {
                long lim = N / i;
                res += lim; //change order of integer.
            }
            res *= 3;

            for (long i = 2; i <=N; i++)
            {
                long lim = N / i;
                Run2(i, 1,lim, 1);
                if (i % 5000 == 0 || (i < 5000 && i % 50 == 0) || (i < 10))
                    Console.WriteLine(i);
            }
            res += N;
            Console.WriteLine("Result" + res);

        }

        static void Run2(long x1, long x0, long lim, long steps)
        {
            if (lim == 0) return;
            steps++;
            //res += TmpSum2(x1, x0, lim, steps);
            AddToRes(TmpSum2(x1, x0, lim, steps));
            var difn = N - x1;
            var x20 = x0;
            for (long i = 0; i < lim; i++)
            {
                x20 += x1;
                if (difn < x20) return;
                Run2(x20, x1, difn / x20, steps);
            }
        }

        private static long sumcount = 0;
        static void AddToRes(long tmpout)
        {
            res += tmpout;
            sumcount = (sumcount + 1)%5000000;
        }


        static long TmpSum(long x1, long x0,long lim, long steps)
        {
            long tmpsum = 0;
            long mystep = (2*steps + 1);
            for (long i = 1; i <= lim; i++)
            {
                tmpsum += N / (x1 * i + x0) ; //change order of integer.
            }
            return tmpsum*mystep;
        }
        static long TmpSum2(long x1, long x0, long lim, long steps) // x0 < x1 always.
        {
            if (lim == 0) return 0;
            long tmpsum = 0;
            long mystep = (2 * steps + 1);
            var lims = (N - x0)/x1;
            for (long i = 1; i <= lims; i++)
            {
                long z = N/(x1*i + x0);
                tmpsum += z; //change order of integer.
            }
            return tmpsum * mystep;
        }


        static void Solve1()
        {
            for (int i = 2; i <= N; i++)
            {
                Run(i, 1, 1);
                Console.WriteLine(i);
            }
            res += N;
            Console.WriteLine("Result" + res);

        }


        static void Run(long x1, long x0,  long steps)
        {
            long lim = N/x1;
            if (lim == 0) return;
            res += lim * (2*steps + 1); //change order of integer.
            for (int i = 1; i <= lim; i++)
                Run(x1 * i + x0, x1, steps + 1);
        }



        static IEnumerable<long> Fibonaci(int x2 = 2)
        {
            int z1 = 1;
            int z2 = x2;
            yield return z1;
            yield return z2;
            while (true)
            {
                z2 = z1 + z2;
                z1 = z2 - z1;
                yield return z2;
            }
        } 

    }
}
