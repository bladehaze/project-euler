﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem28
    {
        private static int N = 100000000;
        static void Main()
        {
            var lis = GenerateDiagnal(1001).Sum();
            var allprimes = MathLibrary.MathLib.FindPrimesBySieveOfAtkins(100000000);
            var primehash = new HashSet<long>(allprimes.Select(e=>(long)e));
            Problem58(primehash);
            Console.WriteLine("Res " + lis);
        }
        static void Problem58(HashSet<long> primehash )
        {
            int primecount = 0;
            int cyclelength = 3;
            foreach (var l in GenerateDiagnal(1000000000000))
            {
                if ((l < N && primehash.Contains(l))||((l>N) && (MathLibrary.MathLib.Isprime(l))))
                    primecount++;
                if(l==cyclelength*cyclelength)
                {
                    var r = primecount*1.0/ ((cyclelength - 1)*2 + 1);
                    if(r<=0.1)
                    {
                        Console.WriteLine(cyclelength);
                        break;
                    }
                    cyclelength += 2;
                }
            }
        }
        
        static IEnumerable<long> GenerateDiagnal(long sidelength)
        {
            long N = (sidelength + 1)/2;
            long starting = 1;
            yield return starting;
            for (int i = 2; i <= N; i++)
            {
                long step = 2*i - 2;
                for (int j = 0; j < 4; j++)
                {
                    starting += step;
                    yield return starting;
                }
            }
        }
    }
}
