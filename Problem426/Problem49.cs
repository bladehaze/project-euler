﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem49
    {
        static void Main()
        {
            var allprime = MathLibrary.MathLib.FindPrimesBySieveOfAtkins(10000);
            var fz = allprime.Where(e => MathLibrary.MathLib.BreakDigits(e).ToList().Count == 4).ToList();

            foreach (var i in fz)
            {
                var z = Search(fz, i).ToList();
                if(z.Count<3)continue;
                var p = z.OrderBy(e => e).ToList();
                if(HasN(p.ToArray()))
                {
                    // get the sequence, eye-ball find which one, lazy ass.
                }
            }

        }

        static bool HasN(int[] num)
        {
            var newh = new HashSet<int>(num);
            for (int i = 0; i < num.Length; i++)
            {
                for (int j = i+1; j < num.Length; j++)
                {
                    if (newh.Contains((num[j] + (num[j] - num[i]))))
                        return true;
                }
            }
            return false;
        }

        static IEnumerable<int> Search(IEnumerable<int> data, int thisprime )
        {
            var tt = MathLibrary.MathLib.BreakDigits(thisprime).ToList();
            foreach (var i in data)
            {
                var ke = new HashSet<int>(tt);
                var newd = ke.ToDictionary(e => e, e => tt.Count(f => f == e));
                var tempt = MathLibrary.MathLib.BreakDigits(i).ToList();
                if(tempt.Any(e=>!newd.ContainsKey(e))) continue;
                foreach (var i1 in tempt)
                {
                    newd[i1] -= 1;
                }
                if (newd.Values.All(e => e == 0)) yield return i;
            }
        }

    }
}
