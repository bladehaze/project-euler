﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Mathematics.RationalNumbers;
namespace Problem426
{
    class Problem422
    {

        static void Main()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var xx = Result();
            sw.Stop();
            Console.WriteLine("Time " + sw.ElapsedMilliseconds);
            Console.WriteLine("Res " + xx);
        }

        private static long Result()
        {
            long N = (long)Math.Pow(11L, 14L);
            long MM = 1000000007;
            var pp = CalculateWithKnownPn(N, MM - 1, Pdictionary);
            var qq = -CalculateWithKnownQn(N, MM - 1, Pdictionary, Qdictionary);

            var res = MathLibrary.MathLib.Modpow(2, 2 * pp - 2, MM);
            res = (res + MathLibrary.MathLib.Modpow(3, 2 * qq - 1, MM)) % MM;
            res = (res + MathLibrary.MathLib.Modpow(2, 2 * pp + 2, MM)) % MM;
            var ppqqx = ((MathLibrary.MathLib.Modpow(2, pp - 2, MM) * MathLibrary.MathLib.Modpow(3, qq - 1, MM))) % MM;
            var ppqq = ((MathLibrary.MathLib.Modpow(2, pp, MM) * MathLibrary.MathLib.Modpow(3, qq, MM))) % MM;
            res = (res + ppqq) % MM;
            res = (res + ppqqx) % MM;
            res = (res - MathLibrary.MathLib.Modpow(3, 2 * qq + 1, MM)) % MM;
            return res;
        }
        private static Dictionary<long, long> Pdictionary = new Dictionary<long, long>();
        private static Dictionary<long, long> Qdictionary = new Dictionary<long, long>();
        private static long CalculateWithKnownPn(long pnIndex, long Mod, Dictionary<long, long> dict)
        {
            if (dict.ContainsKey(pnIndex))
                return dict[pnIndex];

            if (pnIndex < 10)
                return CalculatePn(pnIndex);
            if (pnIndex % 2 == 0)
            {
                var zn1 = CalculateWithKnownPn(pnIndex - 1, Mod, dict);
                var zn2 = CalculateWithKnownPn(pnIndex + 1, Mod, dict);
                var zz = (zn1 - zn2) % Mod;
                dict.Add(pnIndex, zz);
                return zz;
            }
            else
            {
                var znn = (pnIndex + 1) / 2;
                var zz = CalculateWithKnownPn(znn, Mod, dict);
                var extra = znn % 2 == 0 ? 1 : -1;
                var z = (zz * zz + 2 * extra) % Mod;
                dict.Add(pnIndex, z);
                return z;
            }
        }
        private static long CalculateWithKnownQn(long pnIndex, long Mod, Dictionary<long, long> pdict, Dictionary<long, long> qdict)
        {
            if (qdict.ContainsKey(pnIndex))
                return qdict[pnIndex];

            if (pnIndex < 10)
                return CalculateQn(pnIndex);
            if (pnIndex % 2 == 0)
            {
                var zn1 = CalculateWithKnownQn(pnIndex - 1, Mod, pdict, qdict);
                var zn2 = CalculateWithKnownQn(pnIndex + 1, Mod, pdict, qdict);
                var zz = (zn1 - zn2) % Mod;
                qdict.Add(pnIndex, zz);
                return zz;
            }
            else
            {
                var znn = (pnIndex + 1) / 2;
                var qq = CalculateWithKnownQn(znn, Mod, pdict, qdict);
                var pp = CalculateWithKnownPn(znn, Mod, pdict);
                var z = (pp * qq) % Mod;
                qdict.Add(pnIndex, z);
                return z;
            }
        }

        //Script...
        private static Tuple<Rational,Rational> TranslateToZ(Tuple<Rational,Rational> point )
        {
            Rational x = point.Item1;
            Rational y = point.Item2;
            return new Tuple<Rational, Rational>((3*x + 4*y)/25, (4*x - 3*y)/25);
        }

        private static Tuple<Rational, Rational> TranslateToX(Tuple<Rational, Rational> point)
        {
            Rational x = point.Item1;
            Rational y = point.Item2;
            return new Tuple<Rational, Rational>((3 * x + 4 * y), (4 * x - 3 * y) );
        }
        private static Tuple<Rational,Rational> MoveToNextPoint(Tuple<Rational,Rational> p1, Tuple<Rational,Rational>p2 )
        {
            var xx = p1.Item1 / p2.Item1;
            var yy = 1 / xx;
            //var yy = p1.Item1*p2.Item1;
            //var xx = 1/yy;


            return new Tuple<Rational, Rational>(xx, yy);
        }
        private static long GenerateRecursive(long a1, long a2, long Mod, long K)
        {
            if (K == 1) return a1;
            if (K == 2) return a2;
            long z = 0;
            for (long i = 3; i <= K; i++)
            {
                z = (a1 - a2 + Mod)%Mod;
                a1 = a2;
                a2 = z;
            }
            return z;
        }

        private static Tuple<Rational,Rational> CalculateNum(int n)
        {
            var G1 = (Math.Sqrt(5) - 1)/2;
            var G2 = (-1 - Math.Sqrt(5))/2;
            var Pn =(long)( Math.Pow(G1, n-1) + Math.Pow(G2, n-1));
            var Qn = (long) ((Math.Pow(G1, n - 1) - Math.Pow(G2, n - 1))/Math.Sqrt(5));
            if(Pn>0)
            {
                var num = (long)Math.Pow(2, Pn);
                var denom = (long)Math.Pow(3, -Qn);
                var r = new Rational(num, denom);
                return new Tuple<Rational, Rational>(r, 1/r);
            }else
            {
                var denom = (long)Math.Pow(2, -Pn);
                var num = (long)Math.Pow(3, Qn);
                var r = new Rational(num, denom);
                return new Tuple<Rational, Rational>(r, 1 / r);
            }
        }

        private static long CalculatePn(long n)
        {
            var G1 = (Math.Sqrt(5) - 1) / 2;
            var G2 = (-1 - Math.Sqrt(5)) / 2;
            return (long)(Math.Pow(G1, n - 1) + Math.Pow(G2, n - 1));
        }
        private static long CalculateQn(long n)
        {
            var G1 = (Math.Sqrt(5) - 1) / 2;
            var G2 = (-1 - Math.Sqrt(5)) / 2;
            return (long) ((Math.Pow(G1, n - 1) - Math.Pow(G2, n - 1))/Math.Sqrt(5));
        }



        private static IEnumerable<Tuple<long,long>> CalculatePToInfinity(long start, long startindex, long mod)
        {
            while (true)
            {
                yield return new Tuple<long, long>(startindex, start);
                startindex = 2*startindex - 1;
                start = (start*start)%mod;
                start -= 2*(start%2 == 0 ? -1 : 1);
            }
        }
        private static void test2()
        {
            //long N = (long)Math.Pow(11L, 14L);
            ////N = 10;
            ////var zz = CalculationIndex(N).ToList();
            //var pp = CalculateWithKnownPn(N, 1000000006, Pdictionary);
            //var qq = CalculateWithKnownQn(N, 1000000006, Pdictionary, Qdictionary);
            //var nn2 = MathLibrary.MathLib.Modpow(2, pp, 1000000007);
            //var nn3 = MathLibrary.MathLib.Modpow(3, -qq, 1000000007);
            //var zz = Fibonaci(N);
            //var zz = CalculatePn(N);
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var xx = Result();
            sw.Stop();
            Console.WriteLine("Time " + sw.ElapsedMilliseconds);
            Console.WriteLine("Res " + xx);
        }

        private static long Fibonaci(long index)
        {
            var G1 = (1 - Math.Sqrt(5) ) / 2;
            var G2 = (1 + Math.Sqrt(5)) / 2;
            return (long) ((Math.Pow(G2, index) - Math.Pow(G1, index))/Math.Sqrt(5));
        }

        private static IEnumerable<long> CalculationIndex(long N)
        {
            yield return N;
            while (N>10)
            {
                N = (N+1)/2;
                yield return N;
            }
        }

        private static void Test()
        {
            var X = new Tuple<Rational, Rational>(new Rational(7, 1), new Rational(1, 1));
            var p1 = new Tuple<Rational, Rational>(new Rational(13, 1), new Rational(61, 4));
            var p2 = new Tuple<Rational, Rational>(new Rational(-43, 6), new Rational(-4, 1));
            var p3 = new Tuple<Rational, Rational>(new Rational(-19, 2), new Rational(-229, 24));
            var p4 = new Tuple<Rational, Rational>(new Rational(1267, 144), new Rational(-37, 12));

            var tx = TranslateToZ(X);
            var z1 = TranslateToZ(p1);
            var z2 = TranslateToZ(p2);
            var z3 = TranslateToZ(p3);
            var z4 = TranslateToZ(p4);

            var c1 = CalculateNum(1);
            var c2 = CalculateNum(2);
            var c3 = CalculateNum(3);
            var c4 = CalculateNum(4);
            var cc4 = CalculateNum(5);
            cc4 = CalculateNum(6);
            var c7 = CalculateNum(11);

            var np = (MoveToNextPoint(z1, z2));
            var np2 = MoveToNextPoint(z2, z3);
            bool a = MathLibrary.MathLib.Isprime(1000000007);
            //long nnn = (long)Math.Pow(11L, 14L);
            //long res = GenerateRecursive(2, -1, 1000000006L, nnn);
            var nb = TranslateToX(tx);
            //long nn =(long) Math.Pow(11L, 14L)+1;
            long nn = 8;
            for (int i = 3; i < nn; i++)
            {
                np = (MoveToNextPoint(z1, z2));
                z1 = z2;
                z2 = np;
            }

            var nzx = TranslateToX(np);
            Console.WriteLine(nzx);
        }



    }
}
