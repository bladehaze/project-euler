﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem11
    {
        private const string Problem11data =
    @"D:\SourceCodeBackup\HangGitHub\EulerProject\Problem426\Resource\Problem11Data.txt";
        static void Main()
        {
            var digits = ReadDigits(Problem11data).ToList();
            var ma = new long[20,20];
            int ind = 0;
            for (int i = 0; i < digits.Count; i++)
            {
                for (int j = 0; j < digits.Count; j++)
                {
                    ma[i, j] = digits[i][j];
                    Console.Write(digits[i][j]);
                    Console.Write(" ");
                }
                Console.WriteLine();
                ind++;
            }
            var maxl = Enumeration(20, 20, 4).ToList();
            var max = maxl.Select(g => GetValue(g, ma)).ToList();
            
            var mm = max.Max();
            var qq =  max.IndexOf(mm);
            var zz = maxl[qq];
        }
        static IEnumerable<long[]> ReadDigits(string filename)
        {
            var lines = LinqExtension.ReadFile.ReadLine(filename);
            foreach (var line in lines)
            {
                var z = line.Split(' ').Select(g => long.Parse(g)).ToArray();
                yield return z;
            }
        }
        static IEnumerable<IEnumerable<Tuple<int,int>>> Enumeration(int a,int b,int l)
        {
            int z = l - 1;
            for (int i = 0; i < a; i++)
            {
                for (int j = 0; j < b; j++)
                {
                    if(i+z < a)
                        yield return Enumerable.Range(0, 4).Select(g => new Tuple<int, int>(g + i, j)).ToList();
                    if (j + z < b)
                        yield return Enumerable.Range(0, 4).Select(g => new Tuple<int, int>(i, g + j)).ToList();
                    if (i + z < a && j + z < b)
                        yield return Enumerable.Range(0, 4).Select(g => new Tuple<int, int>(i + g, g + j)).ToList();
                    if (i + z < a && j -z >=0)
                        yield return Enumerable.Range(0, 4).Select(g => new Tuple<int, int>(i + g, j-g)).ToList();
                }
            }
        }
        static long GetValue(IEnumerable<Tuple<int,int>> list, long[,] data )
        {
            return list.Select(g => data[g.Item1, g.Item2]).Aggregate((cur, x) => cur*x);
        }

    }
}
