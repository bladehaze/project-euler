﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqExtension;

namespace Problem426.Resource
{
    class Problem81
    {
        static string File = @"D:\SourceCodeBackup\HangGitHub\EulerProject\Problem426\Resource\Problem81Data.txt";
        static string File82 = @"D:\SourceCodeBackup\HangGitHub\EulerProject\Problem426\Resource\Problem82Data.txt";
        static void Main()
        {
            Prob82();
        }

        static void Prob81()
        {
            var m = LinqExtension.ReadFile.ReadMatrix(File, ',', int.Parse);
            var zz = Enumerate(m.GetLength(0)).ToList();
            int N = m.GetLength(0);
            foreach (var tuple in zz)
            {
                int I = tuple.Item1;
                int J = tuple.Item2;
                int right = I + 1 > N - 1 ? int.MaxValue : m[I + 1, J];
                int down = J + 1 > N - 1 ? int.MaxValue : m[I, J + 1];
                var q1 = Math.Min(right, down);
                m[I, J] = m[I, J] + (q1 == int.MaxValue ? 0 : q1);
            }
            Console.WriteLine(m[0, 0]);
        }
        static void Prob82()
        {
            var m = LinqExtension.ReadFile.ReadMatrix(File82, ',', int.Parse);
            var bp = new int[m.GetLength(0),m.GetLength(1)];
            int N = m.GetLength(0);
            for (int i = 0; i < N; i++)
            {
                bp[i, N - 1] = m[i, N - 1];
            }

            for (int j = N - 2; j >= 0; j--)
                for (int i = 0; i < N; i++)
                {
                    //bp[i,j] = Enumerable.Range(0, N).Select(q => m[q, j] + bp[q, j + 1]).Min();
                }






            //for (int j = N-2; j >=0; j--) // column
            //    for (int i = 0; i < N; i++) // row
            //    {
            //        int rown1 = i - 1;
            //        int row = i;
            //        int rowp1 = i + 1;
            //        int rn1 = rown1 < 0 ? int.MaxValue : (m[rown1, j] + bp[rown1, j + 1]);
            //        int rn = ( bp[row, j + 1]);
            //        int rp1 = rowp1 >= N ? int.MaxValue : (m[rowp1, j] + bp[rowp1, j + 1]);
            //        bp[i, j] = m[i, j] + Math.Min(rn1, Math.Min(rn, rp1));
            //    }
            var mins = Enumerable.Range(0, N).Select(p => bp[p, 0]).Min();
            var mins1 = Enumerable.Range(0, N).Select(p => m[p, 0]).Min();
            Console.WriteLine(mins);
        }

        static IEnumerable<Tuple<int,int>> Enumerate(int size)
        {
            var z1 = Enumerable.Range(0, size)
                .Reverse()
                .SelectMany(f => Enumerable.Range(0,size).Where(q=>f+q<size).Select(p=>new Tuple<int, int>(size-1-p,f+p)));
            foreach (var tuple in z1)
            {
                yield return tuple;
            }
            foreach (var VARIABLE in z1.Reverse())
            {
                if(VARIABLE.Item1+VARIABLE.Item2==size-1)continue;
                yield return new Tuple<int, int>(size - 1 - VARIABLE.Item1, size - 1 - VARIABLE.Item2);
            }
        }

    }
}
