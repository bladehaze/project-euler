﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem13
    {
        private const string Problem11data =
            @"D:\SourceCodeBackup\HangGitHub\EulerProject\Problem426\Resource\Problem13.txt";
        static void Main()
        { 
            
        }
        static IEnumerable<long[]> ReadDigits(string filename)
        {
            var lines = LinqExtension.ReadFile.ReadLine(filename);
            foreach (var line in lines)
            {
                var z = line.Split(' ').Select(g => long.Parse(g)).ToArray();
                yield return z;
            }
        }
    }
}
