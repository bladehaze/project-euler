﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem204
    {
        private static int M = 100;
        private static double Maxboundary = 9 * Math.Log(10);
        //private static double Maxboundary = Math.Log(15);
        private static double[] _logp;
        private static int[] _cout; 
        static void Main()
        {
            
            var allprime = MathLibrary.MathLib.FindPrimesBySieveOfAtkins(M);
            var allpower = allprime.Select(g => (int) (Maxboundary/Math.Log(g))).ToList();
            _cout = allpower.ToArray();
            var logp = allprime.Select(g => Math.Log(g)).ToList();
            _logp = logp.ToArray();
            var date = DateTime.Now;
            var res = Count(_logp, _cout, Maxboundary,1);
            
            var to = DateTime.Now.Subtract(date).TotalMilliseconds;
            Console.WriteLine("Result " + res);
            Console.WriteLine("Time(msc) " + to);

            // Forum solution, claimed 0.25 sec, test to be 0.5 sec slower than mine.
            date = DateTime.Now;
            // primes < 100, 25 of them
            ulong[] primes = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 };

            ulong max = 1000000000;

            ulong count = CountHamming(1, primes, 0, max) + 1; // +1 for last value

            Console.WriteLine("{0} generalized Hamming numbers less than {1} of type {2}", count, max + 1, primes.Last());
            to = DateTime.Now.Subtract(date).TotalMilliseconds;
            Console.WriteLine("Time2(msc) " + to);
        }

        static long Count(double[] logp, int[] count, double max,int n  )
        {
            if(logp.Length==n)
            {
                var res = max/logp[n-1]+1;
                return (long) res;
            }
            long sum = 0;
            var newmax = max;
            for (int i = 0; i <= count[n-1]; i++)
            {
                if(newmax<0)
                    break;
                var tmpc = Count(logp, count, newmax,n+1);
                sum += tmpc;
                newmax -= logp[n-1];
            }
            return sum;
        }
        static ulong CountHamming(ulong hamming, ulong[] primes, int start, ulong max)
        {
            ulong count = 0;
            while (hamming <= max)
            {
                if (start + 1 < primes.Length)
                    count += CountHamming(hamming, primes, start + 1, max);
                hamming *= primes[start];
                if (hamming <= max)
                    count++;
            }
            return count;
        }

    }
}
