﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem31
    {
        static void Main()
        {
            var blocks = new double[8] {1, 2, 5, 10, 20, 50, 100, 200};
            var p = MathLibrary.MathLib.CountPossibility(blocks, 200);
            var q = MathLibrary.MathLib.CountPossibility(blocks, 199);
            Console.WriteLine(p-q);
        }
    }
}
