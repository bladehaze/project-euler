﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem427
    {
        
        static void Main()
        {
            int N = 3;
            cache = new long[N+1,N+1];
            var res0 = SnmFunction(3, 2);
            var res = GetResult(N);
        }
        static long GetResult(long n )
        {
            long sum1 = 0;
            for (int i = 0; i < n; i++)
            {
                sum1 += SnmFunction(n, i+1);
            }
            return sum1;
        }
        private static long[,] cache;

        static long SnmFunction(long n, long m) // no mod
        {
            if (n <= 0 || m<=0) return 0;

            if (m > n) return 0;
            if (m == n) return n;
            if(cache[m,n]==0)
            {
                if(m ==1 )
                {
                    cache[m, n] = n*(long) Math.Pow(n - 1, n - 1);
                    return cache[m, n];
                }else
                {
                    long sum = 0;
                    for (int i = 1; i <= m-1; i++)
                    {
                        sum += MathLibrary.MathLib.Choose(n, n - i)*SnmFunction(n - 1, m);
                    }
                    long sum2 = 0;
                    for (int i = 1; i <=m; i++)
                    {
                        sum2 += SnmFunction(n - m, i);
                    }
                    long res = (sum + sum2*MathLibrary.MathLib.Choose(n, n - m))*(n - 1);
                    cache[m, n] = res;
                    return res;
                }
            }
            else
            {
                return cache[m, n];
            }
        }

    }
}
