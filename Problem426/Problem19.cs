﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem19
    {
        static void Main()
        {
            var start = new DateTime(1901, 1, 1);
            var end = new DateTime(2000, 12, 31);
            var sum = 0;
            while (start<=end)
            {
                if (start.DayOfWeek == DayOfWeek.Sunday && start.Date.Day == 1)
                {
                    sum++;
                }
                start =  start.AddDays(1);
            }
            Console.WriteLine(sum);
        }
    }
}
