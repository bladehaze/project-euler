﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using LinqExtension;
namespace Problem426
{
    class Problem8
    {// also problem 13
        private const string Problem8data =
            @"D:\SourceCodeBackup\HangGitHub\EulerProject\Problem426\Resource\Problem8Data.txt";
        private const string Problem13data =
    @"D:\SourceCodeBackup\HangGitHub\EulerProject\Problem426\Resource\Problem13.txt";
        static void Main()
        {
            var lists = ReadDigits(Problem8data).ToList();
            var res = lists.ActionOnMovingWindow(x => x.Product() , 5).Max();
            Console.WriteLine("8 Res " + res);

            var list = ReadDigits(Problem13data).ToList();
            var res2 = list.Select(g => (long) g).Aggregate((cur, x) => ((long) cur + (long) x)%10000000000L);
            Console.WriteLine("13 res " + res2);
        }
        static IEnumerable<int> ReadDigits(string filename)
        {
            var lines = LinqExtension.ReadFile.ReadLine(filename);
            return from line in lines from VARIABLE in line select int.Parse(VARIABLE.ToString());
        }
    }
}
