﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem10
    {
        static void Main()
        {
            var sw = new Stopwatch();
            sw.Start();
            List<long> knownprim = new List<long>(MathLibrary.MathLib.Allprime().Take(350).ToList());
            long maxprime = knownprim.Max();
            long next = maxprime + 1;
            long sum = knownprim.Sum();
            long total = 350;
            while (next < 2000000)
            {
                var sqrt = Math.Sqrt(next)+1;
                if(knownprim.TakeWhile(e=>e<sqrt).Any(f=>next%f==0))
                {
                    next++;
                    continue;
                }
                knownprim.Add(next);
                sum += next;
                total++;
                next++;
            }
            sw.Stop();
            Console.WriteLine("Time "+sw.ElapsedMilliseconds + " Result " + sum);


            var res = MathLibrary.MathLib.AllPrime(2000000).Sum();
            Console.WriteLine("Result " + res);
        }
    }
}
