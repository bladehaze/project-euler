﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Program
    {
        static void Main(string[] args)
        {
            var sw = new Stopwatch();
            sw.Start();
            var linkedl = new LinkedList<long>(InitalConditionT().Take(10000001));
            var sols = SolutionEfficient(linkedl).Sum(g=>g*g);
            sw.Stop();
            Console.WriteLine("Time " + sw.ElapsedMilliseconds);
            Console.WriteLine("solution " + sols);
        }

        static IEnumerable<long> SolutionEfficient(LinkedList<long> data   )
        {
            var itor = data.First;
            while (itor.Next!=null)
            {
                if(itor.Value<=itor.Next.Value)
                {
                    yield return itor.Value;
                    if(itor.Previous!=null)
                    {
                        itor.Previous.Value += itor.Next.Value - itor.Value;
                    }
                    data.Remove(itor.Next);
                    data.Remove(itor);
                    itor = data.First;
                }
                else
                {
                    itor = itor.Next;
                }
            }
            foreach (var VARIABLE in LinqExtension.LinqExtension.EveryOther(data))
            {
                yield return VARIABLE;
            }
        }


        private static IEnumerable<long> IntialConditionS()
        {
            long s0 = 290797;
            while (true)
            {
                yield return s0;
                s0 = (s0*s0)%50515093L;
            }
        } 
        private static IEnumerable<long> InitalConditionT()
        {
            return IntialConditionS().Select(i => (i%64L) + 1);
        }


        static IEnumerable<long> Solution(LinkedList<long> data)
        {
            while (data.First != null)
            {
                var z = MarketLinkedList(data.First);
                if (z == null)
                {
                    yield return data.Last.Value;
                    data.Remove(data.Last);
                    if (data.First != null) data.Remove(data.Last);
                }
                else
                {
                    yield return z.Value;
                    if (z.Previous != null)
                        z.Previous.Value += z.Next.Value - z.Value;
                    data.Remove(z.Next);
                    data.Remove(z);
                }
            }
        }
        static LinkedListNode<long> MarketLinkedList(LinkedListNode<long> data)
        {
            while (data.Next != null)
            {
                if (data.Value <= data.Next.Value)
                    return data;
                data = data.Next;
            }
            return null;
        }




        static void runsim()
        {
            var taz = InitalConditionT().Take(10000001).ToList();
            var tz = taz.ToArray();
            int i = 0;
            while (true)
            {
                //int zq = SkipCount(tz);
                //tz = tz.Skip(zq).ToList();
                //tz = MoveOnce(tz).ToList();
                MoveOneWithNoIterator(tz, tz.Length);
                Console.WriteLine("Count " + i);
                i++;
                if (i < 100)
                    continue;
                //var ct = MinCollision(tz, tz.Length);
                //if (ct == 1) continue;
                //ReduceSpace(tz, tz.Length, ct);
                if (Stoped(tz))
                {
                    var finalresult = LinqExtension.LinqExtension.EveryOther(tz).ToList();
                    var z1 = LinqExtension.LinqExtension.EveryOther(tz).Sum(e => e * e);
                    Console.WriteLine("Stoped " + z1);
                    break;
                }
            }
        }
        static IEnumerable<long> Solution(long[] tz)
        {
            for (int j = 0; j < (tz.Length + 1) / 2; j++)
            {
                var n1 = MarkReduceSolitons(tz, tz.Length);
                if (n1 == 0) break;
                yield return n1;
            }
            yield return tz[0];
        } 
        static long MarkReduceSolitons(long[] data, int len )
        {
            for (int i = 0; i < len-1; i++)
            {
                if(data[i]!=0 && data[i]<=data[i+1]) // first soliton.
                {
                    var outp=  data[i];
                    Renormalize(data, i, len);
                    return outp;
                }
            }
            return 0;
        }
        static void Renormalize(long[] data,int index, int len)
        {
            if(index==0)
            {
                for (int i = 0; i < len; i++)
                {
                    data[i] = i >= len - 2 ? 0 : data[i + 2];
                }
                return;
            }
            data[index - 1] += data[index + 1] - data[index];
            for (int i = index; i < len; i++)
            {
                data[i] = i >= len - 2 ? 0 : data[i + 2];
            }
        }

        static int SkipCount(IEnumerable<long> data )
        {
            // skip count, balls are monotonic increasing, and emptyspace is larger than previous ball.
            bool isball = true;
            long lastball = 0;
            int count = 0;
            foreach (var l in data)
            {
                if(isball)
                {
                    if (lastball > l)
                        return count;
                    isball = false;
                    lastball = l;
                }
                else
                {
                    if (l < lastball)
                        return count-1;
                    isball = true;
                }
                count++;
            }
            return 0;
        }
        static bool Stoped(IEnumerable<long> data )
        {
            bool isball = true;
            long lastball = 0;
            foreach (var l in data)
            {
                if (isball)
                {
                    if (l < lastball)
                        return false;
                    lastball = l;
                    isball = false;
                    continue;
                }
                else
                {
                    if (lastball > l)
                        return false;
                    isball = true;
                }
            }
            return true;
        }

        static IEnumerable<long> MoveOnce(IEnumerable<long> tz )
        {
            var itor = tz.GetEnumerator();
            itor.MoveNext();
            //return MoveOneTimeHelper(itor, 0).Skip(1).ToList();
            return MoveOneWithNoRecurr(itor, 0).Skip(1).ToList();
        }
        static long CollisionTime(long a, long b, long c)
        {
            
            if (b < a) return 1; // next step collide.
            long step = a - c;
            if (step < 0) return long.MaxValue;//never collide.
            return (int) Math.Floor((b - a)/(double) step) + 1;
        }
        static long MinCollision(long[] data,int len)
        {
            long col = long.MaxValue;
            for (int i = 0; i < len-2; i++)
            {
                var tmp = CollisionTime(data[i], data[i + 1], data[i + 2]);
                if(tmp<col)
                {
                    col = tmp;
                }
                if(col==1)
                    break;
            }
            return col;
        }
        static void ReduceSpace(long[] data, int len, long collisiontime)
        {
            var rednum = collisiontime - 1;
            if (rednum == 0) return;
            for (int i = 1; i < len; i+=2)
            {
                var step = data[i - 1] - data[i + 1];
                data[i] -= step*rednum;
            }
        }
        static void MoveOneWithNoIterator(long[] data,int len)
        {
            bool curentisball = true;
            long unassignedballs = 0;
            long preemptybox = 0;
            for (int i = 0; i < len; i++)
            {
                long curnum = data[i];
                if(i==0)
                {
                    curentisball = false;
                    unassignedballs = curnum;
                    preemptybox = 0;
                    continue;
                }
                
                if(!curentisball)
                {
                    if(curnum<unassignedballs)
                    {
                        data[i - 1] = curnum;
                        unassignedballs -= curnum;
                        curentisball = true;
                        preemptybox = 0;
                        continue;
                    }else
                    {
                        data[i - 1] = unassignedballs;
                        preemptybox = curnum - unassignedballs;
                        unassignedballs = 0;
                        curentisball = true;
                    }
                }else
                {
                    unassignedballs += curnum;
                    curentisball = false;
                    data[i - 1] = curnum + preemptybox;
                    preemptybox = 0;
                }
            }
            data[len - 1] = unassignedballs;
        }


        static IEnumerable<long> MoveOneWithNoRecurr(IEnumerator<long> z1, long preemptybox)
        {
            bool curentisball = false;
            long unassignedballs = z1.Current;
            yield return preemptybox + unassignedballs;
            while (z1.MoveNext())
            {
                long i = z1.Current;
                if(i<0)
                    throw new ArithmeticException("Overflow.");
                if( !curentisball) // if current is empty.
                {
                    if (i < unassignedballs) // not enough to assign all balls.
                    {
                        yield return i; // must be all balls.
                        unassignedballs = unassignedballs - i;// place this in empty box.
                        curentisball = true;
                        continue;
                    }
                    else
                    {//evaluation restarts.
                        yield return unassignedballs;
                        preemptybox = i - unassignedballs;
                        unassignedballs = 0;
                        curentisball = true;
                    }
                }else
                {
                    unassignedballs += i;
                    curentisball = false;
                    yield return i+preemptybox;
                    preemptybox = 0;
                }
            }
            if(unassignedballs>0)
                yield return unassignedballs;
        }


        static IEnumerable<long> MoveOneTimeHelper(IEnumerator<long> z1, long preemptybox)
        {
            bool curentisball = false;
            long unassignedballs = z1.Current;
            yield return preemptybox + unassignedballs;
            while (z1.MoveNext())
            {
                long i = z1.Current;
                if( !curentisball) // if current is empty.
                {
                    if (i < unassignedballs) // not enough to assign all balls.
                    {
                        yield return i; // must be all balls.
                        unassignedballs = unassignedballs - i;// place this in empty box.
                        curentisball = !curentisball;
                        continue;
                    }
                    else
                    {//evaluation restarts.
                        yield return unassignedballs;
                        var prem = i - unassignedballs;
                        unassignedballs = 0;
                        z1.MoveNext();
                        foreach (var l in MoveOneTimeHelper(z1,prem))
                        {
                            yield return l;
                        }
                    }
                }else
                {
                    unassignedballs += i;
                    curentisball = !curentisball;
                    yield return i;
                }
            }
            if(unassignedballs>0)
                yield return unassignedballs;
        }
        static long FirstIndexOf<T>(IEnumerable<T> data, Func<T,bool> func )
        {
            long ind = 0;
            foreach (var VARIABLE in data)
            {
                if (func(VARIABLE))
                    return ind;
                ind++;
            }
            return -1L;
        }


        private static IEnumerable<bool> Extends(IEnumerable<long> data )
        {
            bool isz = true;
            foreach (var l in data)
            {
                for (int i = 0; i < l; i++)
                {
                    yield return isz;
                }
                isz = !isz;
            }
        }
    }
}
