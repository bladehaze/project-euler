﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Problem426
{
    class Problem423
    {
        private static BigInteger Five = 5;
        private static BigInteger Six = 6;
        private static long[] S5;
        private static long[] Pascal;
        static void Main()
        {
            int N = 50000000;
            int Mod = 1000000007;
            _dictionary = new long[N];
            var allprim = MathLibrary.MathLib.FindPrimesBySieveOfAtkins(N);

            S5 = new long[N];
            //Pascal = Enumerable.Repeat(0, N+1).Select(e => (long) e).ToArray();
            Pascal = new long[N+1];
            Pascal[0] = 1;
            int part = 100;
            Parallel.For(0, N / part, i =>
                                   {
                                       for (int j = i * part; j < (i + 1) * part; j++)
                                       {
                                           S5[j] = MathLibrary.MathLib.Modpow(5, j, Mod);
                                       }
                                   });



            Console.WriteLine("finish list primes ");
            var p = SS(N,1000000007,allprim);
            Console.WriteLine("res " + p);
        }

        private static long C(long n)
        {
            var pi= MathLibrary.MathLib.Allprime().TakeWhile(d => d <= n).Count();
            var sum1 = BigInteger.Pow(Five,  (int)n - 1)*6; //power function failed.
            for (int j = 0; j < pi; j++)
            {
                var i = j + 1;
                var tmp = 6*(long)Math.Pow(5, n - i - 1);
                // now chose i numbers(replacable) to repeat once.
                sum1 += tmp * MathLibrary.MathLib.Choose(n-1,i);
            }
            return (long)sum1;
        }

        private static long S(long n, long mod, List<int> pi )
        {
            _dictionary = new long[n];
            long res = 0;
            for (int i = 0; i < n; i++)
            {
                res = (res + C(i+1, mod,pi.TakeWhile(e=>e<=i+1).Count()))%mod;
                if(i%5000==0)
                    Console.WriteLine(i);
            }
            return res;
        }

        private static long[] _dictionary;
        private static long C(long n, long mod,long pi)
        {
            //var pi = MathLibrary.MathLib.Allprime().TakeWhile(d => d <= n).Count();
            //var sum1 = BigInteger.Pow(Five, (int)n - 1) * 6; //power function failed.
            //if (n <= 1) return 0;
            //var sum1 = (MathLibrary.MathLib.Modpow(5, n - 1, mod)*6)%mod;
            var sum1 = (S5[n-1]*6)%mod;
            for (int j = 0; j < pi; j++)
            {
                var i = j + 1;
                long tmp = 0;
                if (_dictionary[n - i - 1] != 0)
                    tmp = _dictionary[n - i - 1];
                else
                {
                    //tmp = (6 * MathLibrary.MathLib.Modpow(5, n - i - 1,mod))%mod;
                    tmp = (6*S5[n - i - 1])%mod;
                    _dictionary[n - i - 1] = tmp;
                }
                // now chose i numbers(replacable) to repeat once.
                var tt = ((tmp%mod)*(MathLibrary.MathLib.Choose(n - 1, i)%mod))%mod;
                sum1 = (sum1+ tt)%mod;
                //sum1  =sum1% mod;
            }
            
            return (long)sum1;
        }

        private static long C2(long n, long mod, long pi, long[] prefactor)
        {
            //var pi = MathLibrary.MathLib.Allprime().TakeWhile(d => d <= n).Count();
            //var sum1 = BigInteger.Pow(Five, (int)n - 1) * 6; //power function failed.
            //if (n <= 1) return 0;
            //var sum1 = (MathLibrary.MathLib.Modpow(5, n - 1, mod)*6)%mod;
            var sum1 = (S5[n - 1] * 6) % mod;
            long smallerfactor =n-1==0?0: prefactor[0];
            var max = Math.Min(pi+20, n - 1);
            for (int j = 0; j < max; j++)
            {
                var i = j + 1;

                // now chose i numbers(replacable) to repeat once.
                //var shouldbe = MathLibrary.MathLib.Choose(n - 1, i);
                int i1 = i ;
                long f1 = smallerfactor;
                long f2 = prefactor[i1];
                smallerfactor = (f1 + f2) % mod;
                prefactor[i1] = smallerfactor;
                var ff = smallerfactor;
                smallerfactor = f2;
                if(j>=pi)continue;
                long tmp = 0;
                if (_dictionary[n - i - 1] != 0)
                    tmp = _dictionary[n - i - 1];
                else
                {
                    //tmp = (6 * MathLibrary.MathLib.Modpow(5, n - i - 1,mod))%mod;
                    tmp = (6*S5[n - i - 1])%mod;
                    _dictionary[n - i - 1] = tmp;
                }
                var tt = ((tmp % mod) * ff) % mod;
                sum1 = (sum1 + tt) % mod;
            }

            return (long)sum1;
        }


        private static long SS(long n, long mod, List<int> primes )
        {
            var itorprime = primes.GetEnumerator();
            itorprime.MoveNext();
            bool noprime = false;
            long pinum = 0;
            long res = 0;
            for (int i = 1; i <= n; i++)
            {
                if (!noprime && itorprime.Current <= i)
                {
                    pinum++;
                    noprime = !itorprime.MoveNext();
                }
                res = (res + C2(i, mod, pinum,Pascal))%mod;
                if (i % 5000 == 0)
                    Console.WriteLine(i);
            }
            return res;
        }



    }
}
