﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Problem426
{
    class Problem185
    {
        private const string Problemdata =
    @"D:\SourceCodeBackup\HangGitHub\EulerProject\Problem426\Resource\Problem185.txt";

        private static List<int[]> Guesses = new List<int[]>();
        private static List<int> Correct = new List<int>(); 
        static void Main()
        {
            ReadData();

            List<HashSet<int>> used = Guesses.First().Select(e => new HashSet<int>(Enumerable.Range(0, 10))).ToList();

            var zz = Choose(Enumerable.Range(0, 16).ToList(), 2).Select(e=>new Tuple<int,int>(e.First(),e.Last())).ToList();
            var zzz = new HashSet<Tuple<int, int>>(zz);
            int[] pastguess = Enumerable.Repeat(-1, Guesses.First().Count()).ToArray();
            bool[] guessed = new bool[pastguess.Length];

            bool xx= CurrentGuess(pastguess, guessed, 0,used);
            NoOneGuess(pastguess);
            CheckAll(pastguess);
            var res = pastguess.Zip(Enumerable.Range(0, 16).Select(e => (long)Math.Pow(10, e)).Reverse(), (x, y) => x*y).Sum();
            Console.WriteLine(res);
        }
        //static bool PastGuess(int[] guess, bool[] guessed, List<HashSet<int>> used, int ind)
        //{
        //    var thisgu = Guesses[ind];
        //    var cor = Correct[ind];
        //    for (int i = 0; i < thisgu.Length; i++)
        //    {
        //        if (guess[i] == thisgu[i])
        //            cor--;
        //    }
        //    if (cor < 0)
        //        return false;
        //    foreach (var VARIABLE in Choose(guessed, cor))
        //    {
        //        if (VARIABLE.Any(e => !used[e].Contains(thisgu[e])))
        //            continue;

        //    }
        //}




        static bool CheckAll(int[] guess)
        {
            for (int i = 0; i < Guesses.Count; i++)
            {
                var thisgu = Guesses[i];
                var count = 0;
                for (int j = 0; j < Guesses.First().Count(); j++)
                {
                    if (guess[j] == thisgu[j])
                        count++;
                }
                if (count != Correct[i])
                    return false;
            }
            return true;
        }

        static void NoOneGuess(int[] guess)
        {
            if (guess.All(e => e >= 0)) return;
            var index = guess.Select((e, i) => new {e, i}).Where(f => f.e < 0).Select(g => g.i).ToList();
            foreach (var i in index)
            {
                var h = new HashSet<int>(Enumerable.Range(0, 10));
                foreach (var intse in Guesses)
                {
                    h.Remove(intse[i]);
                }
                guess[i] = h.First();
            }
        }

        static bool CurrentGuess(int[] pastguess, bool[] guessed, int ind, List<HashSet<int>> used )
        {
            if (ind == Guesses.Count)
                return true;
            var thisgues = Guesses[ind];
            int cor = Correct[ind];
            for (int i = 0; i < guessed.Length; i++)
            {
                if (guessed[i] && pastguess[i] == thisgues[i])
                {
                    cor--;
                    continue;
                }
            }
            if (cor < 0)
                return false;
            if (cor == 0)
            {
                var skipid = new HashSet<int>();
                for (int i = 0; i < thisgues.Length; i++)
                {
                    if (used[i].Contains(thisgues[i]))
                    {
                        used[i].Remove(thisgues[i]);
                        skipid.Add(i);
                    }
                }
                if(CurrentGuess(pastguess, guessed, ind + 1, used))
                    return true;
                else
                {
                    foreach (var i in skipid)
                    {
                        used[i].Add(thisgues[i]);
                    }
                    return false;
                }
            }

            int left = guessed.Count(e => !e);
            if(left<cor)
                return false;
            foreach (var VARIABLE in Choose(guessed,cor))
            {
                var tt = VARIABLE.ToList();
                if (tt.Any(e => !used[e].Contains(thisgues[e])))
                    continue;
                foreach (var i in VARIABLE)
                {
                    used[i].Remove(thisgues[i]);
                    pastguess[i] = thisgues[i];
                    guessed[i] = true;
                }


                var newh = new HashSet<int>(VARIABLE);
                var skipadd = new HashSet<int>();
                //var moreguess = new HashSet<int>();
                //for (int i = 0; i < Guesses.First().Length; i++)
                //{
                //    if (used[i].Count == 1 && !guessed[i])
                //        pastguess[i] = used[i].First();
                //}
                for (int i = 0; i < Guesses.First().Length; i++)
                {
                    if(newh.Contains(i))
                        continue;
                    if(!used[i].Contains(thisgues[i]))
                    {
                        skipadd.Add(i);
                    }
                    used[i].Remove(thisgues[i]);
                }

                if(!CurrentGuess(pastguess,guessed,ind+1,used))
                {
                    //foreach (var i in moreguess)
                    //{
                    //    pastguess[i] = -1;
                    //    guessed[i] = false;
                    //}
                    foreach (var i in VARIABLE)
                    {
                        used[i].Add(thisgues[i]);
                        pastguess[i] = -1;
                        guessed[i] = false;
                    }
                    for (int i = 0; i < Guesses.First().Length; i++)
                    {
                        if (newh.Contains(i))
                            continue;
                        if(skipadd.Contains(i))
                            continue;
                        used[i].Add(thisgues[i]);
                    }
                    continue;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        static IEnumerable<IEnumerable<int>> Choose(bool[] guessed, int num)
        {
            var index = guessed.Select((e, i) => new {e, i}).Where(f => !f.e).Select(g => g.i).ToList();
            return Choose(index, num);
        } 
        static IEnumerable<IEnumerable<int>> Choose(IEnumerable<int> data, int N )
        {
            if(N==0)
                yield break;
            if(N==1)
                foreach (var i in data)
                {
                    yield return new List<int>(){i};
                }
            int z = data.Count();
            for (int i = 0; i < z-N+1; i++)
            {
                int p = data.Skip(i).First();
                foreach (var i1 in Choose(data.Skip(i+1),N-1))
                {
                    yield return LinqExtension.LinqExtension.AppendRange(p, i1);
                }
            }
        }
         

        static void ReadData()
        {
            Regex pattern = new Regex(@"(?<Guess>\d+) ;(?<Correct>\d+) correct");
            foreach (var line in LinqExtension.ReadFile.ReadLine(Problemdata))
            {
                var o = pattern.Match(line);
                Guesses.Add(o.Groups["Guess"].Value.Select(g => int.Parse(g.ToString())).ToArray());
                int num = int.Parse(o.Groups["Correct"].Value);
                Correct.Add(num);
            }
            ;
        }


    }
}
