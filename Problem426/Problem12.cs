﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem12
    {
        static void Main()
        {
            int N = 3000000;
            var primes = MathLibrary.MathLib.FindPrimesBySieveOfAtkins(N);
            for (int i = 7; i < N; i++)
            {
                int qq = i*(i + 1)/2;
                var dict = MathLibrary.MathLib.Factor(qq, primes);
                var num = MathLibrary.MathLib.NumberofFactor(dict);
                if(num>=500)
                {
                    Console.WriteLine(qq);
                    return;
                }
            }
        }
    }
}
