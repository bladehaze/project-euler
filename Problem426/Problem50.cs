﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem50
    {
        static void Main()
        {
            var st = DateTime.Now;
            int N = 1000000;
            int init = 21;
            var allp = MathLibrary.MathLib.FindPrimesBySieveOfAtkins(N);
            var primehash = new HashSet<long>(allp.Select(e=>(long)e));
            ;
            long maxprime = 0;
            int mxlength = init;
            for (int maxknown = init; maxknown < allp.Count; maxknown += 1)
            {
                var sumas = Enumerable.Range(0, maxknown).Select(e => (long)allp[e]).ToList();
                var suma = sumas.Sum();
                for (int i = 0; i < allp.Count - maxknown; i++)
                {
                    if (suma > allp.Last())
                    {
                        if(i==0)
                            goto Finished;
                        break;
                    }
                    if (!primehash.Contains(suma))
                    {
                        goto Continue;
                    }
                    maxprime = suma;
                    mxlength = maxknown;
                    
                Continue:
                    if(maxknown%2==0)
                        break;
                    suma -= allp[i];
                    suma += allp[i + maxknown];
                    continue;
                }
            }
        Finished:
            Console.WriteLine(maxprime);
            Console.WriteLine(mxlength);
            var ends = DateTime.Now.Subtract(st).TotalMilliseconds;
            Console.WriteLine("Time(msc) " + ends);
        }
    }
}
