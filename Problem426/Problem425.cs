﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem425
    {
        static void Main()
        {
            int N = 1000;
            var boolisprime = new bool[N];
            var allinteger = Enumerable.Range(1, N).ToList();
            var allprime = MathLibrary.MathLib.FindPrimesBySieveOfAtkins(N);
            foreach (var i in allprime)
                boolisprime[i - 1] = true;
            var arranged =  Arrange(allprime);
            bool[] istwoconnect = new bool[N];

            foreach (var b in AllChains(boolisprime,istwoconnect,2))
            {
                istwoconnect[b - 1] = true;
            }
            var allz = istwoconnect.Zip(boolisprime, (x, y) => y && !x).Any(e => e);
        }

        static void Mark(List<int> allprime, bool[] isprime , List<int> allinteger,bool[] istwoconnect  )
        {
        }
        static IEnumerable<int> AllChains(bool[] isprime, bool[] istwoconnected, int prime = 2)
        {
            var lis1 = NextNode(prime, isprime, istwoconnected).ToList();
            foreach (var i in lis1)
                yield return i;
            foreach (var i in lis1)
            {
                foreach (var i1 in AllChains(isprime,istwoconnected,i))
                {
                    yield return i1;
                }
            }
        } 


        static IEnumerable<int> NextNode(int prime, bool[] isprime, bool[] istwoconnect)
        {
            if(!isprime[prime-1])
                yield break;
            int z = 1;
            foreach (var b in BreakPrime(prime)) // change one digit.
            {
                int max = 9 - b;
                int min = z==1?1-b: -b;
                foreach (var b1 in Enumerable.Range(min,max-min+1))
                {
                    int cadidate = prime + z*b1;
                    if (shouldnotcount(isprime,istwoconnect,cadidate)) continue;
                    yield return cadidate;
                }
                z = z*10;
            }
            for (int i = 1; i < 10; i++)
            {
                int cadidate = prime + z * i;
                if (shouldnotcount(isprime, istwoconnect, cadidate)) continue;
                yield return cadidate;
            }
            for (int i = 1; i < 10; i++)
            {
                int cadidate = prime*z +  i;
                if (shouldnotcount(isprime,istwoconnect,cadidate)) continue;
                yield return cadidate;
            }
        }
        static bool shouldnotcount(bool[] isprime, bool[] istwoconnect, int cadidate)
        {
            return cadidate<2|| cadidate>isprime.Count()|| istwoconnect[cadidate - 1] || !isprime[cadidate - 1];
        }
        static IEnumerable<int> BreakPrime(int prime)
        {
            while (prime>0)
            {
                int z = prime%10;
                yield return z;
                prime /= 10;
            }
        }

        static List<int[]> Arrange(List<int> primes)
        {
            var Sortbydigits = new List<int[]>();
            int g = 1;
            var z1 = new List<int>();
            foreach (var prime in primes)
            {
                if(prime/g<10)
                    z1.Add(prime);
                else
                {
                    g = g*10;
                    Sortbydigits.Add(z1.ToArray());
                    z1.Clear();
                    z1.Add(prime);
                }
            }
            return Sortbydigits;
        }
    }
}
