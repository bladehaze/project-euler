﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem35
    {
        private static int N = 1000000;
        static void Main()
        {
            var dod = DateTime.Now;
            var allprime = MathLibrary.MathLib.FindPrimesBySieveOfAtkins(N);
            var allc = allprime.Where(e => !IsNotCircularPrime(e)).ToList();
            var primehash = new HashSet<int>(allc);
            var cp = allc.Where(e => !IsNotCircularPrime(e, primehash)).ToList();
            var t = DateTime.Now.Subtract(dod).TotalMilliseconds;
            Console.WriteLine("Res " + cp);
            Console.WriteLine("Time(msc) " + t);
        }
        private static bool IsNotCircularPrime(int n)
        {
            var l = Translate(n).ToList();
            return l.Count != 1 && (l.Any(e => e%2 == 0) || l.Sum()%3 == 0);
        }
        private static bool IsNotCircularPrime(int n,HashSet<int> sets )
        {
            var cc = Circulars(n).ToList();
            return cc.Any(e => !sets.Contains(e));
        }

        private static IEnumerable<int> Translate(int Z)
        {
            while (Z!=0)
            {
                var p = Z%10;
                Z = (Z - p)/10;
                yield return p;
            }
        }
        private static int ITranslate(List<int> Z)
        {
            var p = Enumerable.Range(0, Z.Count).Select(g => (int) Math.Pow(10, g));
            return p.Zip(Z, (x, y) => x*y).Sum();
        } 

        private static IEnumerable<int> Circulars(int Z)
        {
            var myArray = Translate(Z).ToList();
            return myArray.Select((t, e) => myArray
                                                .Skip(e)
                                                .Concat(myArray.Take(e))
                                                .ToList()).Select(o => ITranslate(o));
        }
    }
}
