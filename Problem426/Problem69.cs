﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem69
    {
        private static int N = 1000000;
        static void Main()
        {
            var allprime = MathLibrary.MathLib.FindPrimesBySieveOfAtkins(N);
            long prod = 1;
            foreach (var i in allprime)
            {
                if (prod*i > N) break;
                prod *= i;
            }
            Console.WriteLine("Res " + prod);
        }
    }
}
