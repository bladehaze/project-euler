﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Problem426
{
    class Problem432
    {
        static void Main()
        {
            //var mob = mobius(100000000000);
            //var res =  sumtotient(1000000);
            Console.WriteLine("Crunching. x ");
            var allpr = MathLibrary.MathLib.FindPrimesBySieveOfAtkins(1000000);
            //var tt = totient(9, allpr);
            //var res2 = totientSum(1000000, allpr, 100000000000L);
            //DateTime st = DateTime.Now;
            //long res = 0;
            //for (int i = 0; i < 1000; i++)
            //{
            //    long from = i * 100000000 + 1;
            //    long to = (i + 1) * 100000000;
            //    res = (res + totientSumFast(from, to, allpr)) % 1000000000;
            //    if (i % 10 == 0)
            //    {
            //        Console.WriteLine(i + " Finished");
            //        var totalm = DateTime.Now.Subtract(st).TotalMinutes;
            //        Console.WriteLine("Time " + totalm);
            //        Console.WriteLine("Estimated finish (Hour)" + totalm * (100000 - i) / 10 / 60);
            //        Console.WriteLine(i + " Current " + from + " to " + to + " " + res);
            //        st = DateTime.Now;
            //    }
            //    Console.WriteLine(i);
            //}

            Console.WriteLine("Finish generating prime.");
            //Console.WriteLine("Answer " + res);
            long N1 = 0;
            long N0 = 0;
            var myprims = new LinkedList<long>(allpr.Select(g => (long) g).ToList());
            long sum = 0;
            long mood = 1000000000;
            long maxN = 100000000000;
            long maxarray = 100000000;
            long maxp = myprims.Last();
            while (N1<maxN)
            {
                N0 = N1 + 1;
                long maxa = N0 + maxarray - 1;
                N1 = Math.Min(myprims.Last() * 2 - 1, maxN);
                N1 = Math.Min(N1, maxa);
                var tmp = totientSumFast2(N0, N1, myprims,maxp,out maxp, mood);
                sum = (sum + tmp)%mood;
                Console.WriteLine("ToGo :" +(maxN-N1));
                System.GC.Collect(GC.MaxGeneration);
            }


            Console.WriteLine("Answer " + sum);




            //var res2 = totientSumFast(1, 1000000, allpr);
            //var myprims = allpr.Select(g => (long) g).ToList();
            //var res2 = totientSumFast2(1, 37, myprims);
        }
        const int pref = 92160;

        static void FilterOnce(long[] ints,bool[] isvisited, long[] prims, long from, long to, int skipprim = 7, bool assignprim = true)
        {
            Parallel.For(0, prims.Length, j =>
            {
                long z = prims[j];
                var p = from % z;
                z = p == 0 ? from : (from / z + 1) * z;
                while (z <= to)
                {
                    var first = j < skipprim ? z : ((prims[j] - 1) * (z / prims[j]));
                    isvisited[z - from] = true;
                    if (Interlocked.CompareExchange(ref ints[z - from],
                                                    first, 0) != 0 && j >= skipprim)
                    {
                        bool run = true;
                        while (run)
                        {
                            long zz = Interlocked.Read(ref ints[z - from]);
                            var bb = (zz / prims[j] * (prims[j] - 1));
                            var qq = Interlocked.CompareExchange(ref ints[z - from], bb,
                                                                 zz);
                            run = qq != zz;
                        }
                    }
                    z += prims[j];
                }
            });
            //if (!assignprim) return;
            //for (int i = 0; i < ints.Length; i++)
            //{
            //    if (!isvisited[i])
            //        ints[i] = i + from - 1 == 0 ? 1 : i + from - 1;
            //}
        }


        static long totientSumFast2(long from, long to, LinkedList<long> prims, long maxp, out long newmaxp, long mood = 1000000000000)
        {
            
            var size = (int)(to - from + 1);
            long[] ints = new long[size];
            bool[] isvisited = new bool[size];
            newmaxp = maxp;
            Parallel.ForEach(prims.TakeWhile(g=>g<=to), prime =>
            {
                long z = prime;
                var p = from % z;
                z = p == 0 ? from : (from / z + 1) * z;
                while (z <= to)
                {
                    var first = prime < 18 ? z : ((prime - 1) * (z / prime));
                    isvisited[z - from] = true;
                    if (Interlocked.CompareExchange(ref ints[z - from],
                                                    first, 0) != 0 && prime >= 18)
                    {
                        bool run = true;
                        while (run)
                        {
                       
                             long zz = Interlocked.Read(ref ints[z - from]);
                            var bb = (zz / prime * (prime - 1));
                            var qq = Interlocked.CompareExchange(ref ints[z - from], bb,
                                                                 zz);
                            run = qq != zz;
                        }
                    }
                    z += prime;
                }
            });
            //var maxp = prims.Last.Value;
            long sum = 0;
            for (int i = 0; i < size; i++)
            {
                if (i + from == 1)
                {
                    sum++;
                    continue;
                }
                if (isvisited[i])
                {
                    sum = (sum + ints[i]) % mood;
                }
                else
                {
                    ints[i] = i + from - 1;
                    if(i+from>maxp)
                    {
                        prims.AddLast(i+from);
                        newmaxp = i + from;
                    }
                    sum = (sum + i + from - 1) % mood;
                }
            }
            return (sum * pref) % mood;

        }

        static long totientSumFast(long from, long to, List<int> prims0)
        {
            var hha = new HashSet<long>(prims0.Select(g=>(long)g));
            //var sw = new Stopwatch();
            //var priml = Math.Sqrt(to);
            //var prims = prims0.TakeWhile((e,i) =>i<7|| e < priml + 1).Select(g=>(long)g).ToArray();
            var prims = prims0.Select(g=>(long)g).ToArray();
            int size = (int) (to - from+1);
            long[] ints = new long[size];
            bool[] isvisited = new bool[size];
            //long mood = 1000000000;
            long mood = 1000000000000;
            //ints[0] = from;
            //int pref = 1;
            bool isfirst = true;
            ////while (prims.Any()&&prims.First()<=to/2)
            ////{
            //    FilterOnce(ints, isvisited, prims, from, to,7);
            //    prims =
            //        isvisited.Zip(ints, (x, y) => new {x, y}).Select((g, i) => !g.x ? i + from : 0).Where(
            //            f => f != 1 && f != 0).
            //            ToArray();
            //if(prims.Any())
            //    FilterOnce(ints, isvisited, prims, from, to, 0,false);
            //if (!isvisited[0])
            //    ints[0] = 1;
            ////}

            Parallel.For(0, prims.Length, j =>
                                             {
                                                 long z = prims[j];
                                                 var p = from % z;
                                                 z = p == 0 ? from : (from / z + 1) * z;
                                                 while (z <= to)
                                                 {
                                                     var first = j < 7 ? z : ((prims[j] - 1) * (z / prims[j]));
                                                     isvisited[z - from] = true;
                                                     if (Interlocked.CompareExchange(ref ints[z - from],
                                                                                     first, 0) != 0 && j >= 7)
                                                     {
                                                         bool run = true;
                                                         while (run)
                                                         {
                                                             long zz = Interlocked.Read(ref ints[z - from]);
                                                             var bb = (zz / prims[j] * (prims[j] - 1));
                                                             var qq = Interlocked.CompareExchange(ref ints[z - from], bb,
                                                                                                  zz);
                                                             run = qq != zz;
                                                         }
                                                     }
                                                     z += prims[j];
                                                 }
                                             });
            



            long sum = 0;

            //for (int i = 0; i < size; i++)
            //{
            //    if (i + from == 1)
            //    {
            //        sum++;
            //        continue;
            //    }
            //    if (isvisited[i])
            //    {
            //        sum = (sum + ints[i]) % mood;
            //    }
            //    else
            //    {
            //        //bool z=!hha.Contains(i+from);
            //        var pp = i + from - 1;
            //        ints[i] = i + from - 1;
            //        sum = (sum + i + from - 1) % mood;
            //    }
            //}
            var allrest =
                isvisited.Zip(ints, (x, y) => new {x, y}).Select((g, i) => !g.x ? i + from : 0).Where(f =>f!=1&& f != 0&&!hha.Contains(f)).
                    ToList();

            foreach (var l in ints)
            {
                sum = (sum + l) % mood;
            }
            return (sum*pref)%mood;
        }
 


        static long totientSum(long N, List<int> prims, long mod )
        {
            long sum = 1;
            for (int i = 2; i <= N; i++)
            {
                sum = (sum+ totient(i, prims))%mod;
            }
            return sum;
        }



        static long totient(long N, List<int> prims )
        {
            var mz = (int)Math.Sqrt(N);
            var tt = N;
            foreach (var prim in prims.Where(e=> (tt<=10&&e<=tt )|| (tt>10 && e<=mz)))
            {
                if (N % prim == 0)
                    N = N/prim*(prim - 1);
            }
            return N;
        }


        static long sumtotient(int N)
        {
            var mob = mobius(N);
            long sum = 0;
            for (int i = 1; i <= N; i++)
            {
                sum += mob[i]*(N/i)*(N/i);
            }
            sum /= 2;
            return sum;
        }



        static int[] mobius(long N)
        {
            var mu = new int[N+1];
            mu[1] = 1;
            for (int i = 1; i <= N; ++i)
                for (int j = 2 * i; j <= N; j += i)
                    mu[j] -= mu[i];
            return mu;
        }
    }
}

//var lis = new List<int>() {2,3,5,7,11,13,17};
//foreach (var li in lis)
//{
//    for (int i = 0; i < ints.Length; i++)
//    {
//        long thisone = i + from;
//        //if (thisone % li == 0)
//        //    ints[i] = (li * ints[i]);
//        //else
//        //{
//        //    ints[i] = (ints[i] * (li - 1));
//        //}
//        if (thisone % li != 0)
//            ints[i] = (ints[i] * (li - 1));
//    }
//}
//var zzz = ints.Any(e => e < 0);  
//Parallel.ForEach(lis, li =>
//                          {
//                              int skipindex = 30;
//                              for (int i = 0; i < li; i++)
//                              {
//                                  if (i == skipindex)
//                                      continue;
//                                  else
//                                  {
//                                      long mod1 = (from + i) % li;
//                                      if (mod1 == 0)
//                                      {
//                                          skipindex = 0;
//                                          continue;
//                                      }
//                                      else
//                                      {
//                                          skipindex = (int)(li - mod1);
//                                      }
//                                  }


//                                  int ii = i;
//                                  var f = (li - 1);
//                                  while (ii < ints.Length)
//                                  {
//                                      var run = true;
//                                      while (run)
//                                      {
//                                          long zz = Interlocked.Read(ref ints[ii]);
//                                          var bb = (zz * f);
//                                          var qq = Interlocked.CompareExchange(ref ints[ii], bb, zz);
//                                          run = qq != zz;
//                                      }
//                                      ii += li;
//                                  }
//                              }
//                          });