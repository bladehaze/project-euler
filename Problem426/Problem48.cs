﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace Problem426
{
    class Problem48
    {
        static void Main()
        {
            BigInteger mod = 10000000000L;
            //long mod = 10000000L;
            long N = 1000;
            BigInteger sum = 0;
            for (int i = 1; i <= N; i++)
            {
                var tmp = MathLibrary.MathLib.Modpow(i, i, mod);
                sum = (sum%mod + tmp) % mod;
            }
            Console.WriteLine(sum);
        }
    }
}
