﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem357
    {
        static void Main()
        {
            var allprime = MathLibrary.MathLib.FindPrimesBySieveOfAtkins(100000000);
            var logp = allprime.Skip(1).Select(e => Math.Log(e)).ToArray();
            var count = allprime.Select(e => 1).ToArray();
            var max = Math.Log(100000000/2);
             MathLibrary.MathLib.CountPossibility(logp, count, max, 1);
        }
    }
}
