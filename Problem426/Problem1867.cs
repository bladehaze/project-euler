﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Problem426
{
    class Problem1867
    {
        private const string Problem18data =
    @"D:\SourceCodeBackup\HangGitHub\EulerProject\Problem426\Resource\Problem18Data.txt";
        private const string Problem67data =
@"D:\SourceCodeBackup\HangGitHub\EulerProject\Problem426\Resource\Problem67Data.txt";
        static void Main()
        {
            //var m = LinqExtension.ReadFile.ReadMatrix(Problem18data, ' ', int.Parse);
            var m = LinqExtension.ReadFile.ReadMatrix(Problem67data, ' ', int.Parse);
            var dptable = new int[m.GetLength(0),m.GetLength(1)];
            for (int i = m.GetLength(0)-1; i >=0; i--)
            {
                if(i==m.GetLength(0)-1)
                    for (int j = 0; j < m.GetLength(1); j++)
                        dptable[i, j] = m[i, j];
                else
                {
                    for (int j = 0; j < m.GetLength(1); j++)
                    {
                        if (m[i, j] == 0) break;
                        dptable[i, j] = Math.Max(dptable[i + 1, j], dptable[i + 1, j + 1]) + m[i, j];// problem 18data.
                        
                    }
                }
            }
            Console.WriteLine(dptable[0,0]);
        }
        
    }
}
