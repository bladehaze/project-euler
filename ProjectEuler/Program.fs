﻿// Learn more about F# at http://fsharp.net
open System
// common functions.
let isnotprime n = [2 .. int32(Math.Sqrt(double(n)))] |>  List.exists (fun x -> n % x=0)
let isprime n = not (isnotprime n);
let naturalnumber =  Seq.unfold (fun x -> Some (x,x+1)) 1
let allprime = naturalnumber |>Seq.skip 1 |>Seq.filter isprime
let fibonaci = Seq.unfold (fun (x,y)->Some(x+y, (x+y,x))) (0,1)
let rec powerr n x y = 
    if x = 0 then y
    else powerr n (x-1) (n*y)
let power n x = 
    powerr n x 1
// #1
let solution = [1 .. 999] |> List.filter (fun x -> x % 3 = 0 || x % 5 = 0) |> List.sum |> Console.WriteLine
// #2
let fibo = fibonaci |> Seq.takeWhile (fun x-> x < 4000000) |> Seq.filter  (fun x-> x%2=0) |> Seq.sum |>Console.WriteLine
// #3
let facto = [2 .. 775146] |> List.filter (fun x -> 600851475143L % int64(x) = 0L) |> List.filter isprime |> Seq.max |> Console.WriteLine
// #5 
let rec maxdivisible m n x = 
    if(m % n <> 0) then x 
    else maxdivisible (m/n) n (x+1) 
let maxfac x y =  x|> Seq.map (fun q -> maxdivisible q y 0) |> Seq.max
let smallcommon seq = seq |> Seq.filter isprime |> Seq.map (fun z-> power z (maxfac seq z)) |> Seq.fold (*) 1 
Console.WriteLine (smallcommon [2 .. 20])

// #6 
let sol = [1 .. 100] |> Seq.map (fun x->x*x) |> Seq.sum
let res = ([1 .. 100] |> Seq.sum|> fun x->x*x) - sol |> Console.WriteLine

//# 7
Console.WriteLine (allprime |> Seq.nth(10000)) // 0th is 2

//#9 
let p = 
    [1 .. 1000] 
    |> Seq.filter (fun x -> [1 .. (1000-x-1)] |> Seq.exists (fun y-> (power x 2) + (power y 2) = power (1000-x-y) 2 ||  (power x 2) = (power y 2) + power (1000-x-y) 2) ) 
    |> Seq.fold (*) 1
Console.WriteLine(p)

//#10
let p2 = allprime |> Seq.takeWhile (fun x -> x<2000000) |> Seq.sum |> Console.WriteLine

//# 426

let initiate s0 K= Seq.unfold (fun x-> Some(x,(x*x)%K)) s0
let init0 N = initiate 290797 50515093 |> Seq.take(N) |> Seq.map (fun x -> x%64+1)
let Rep n x = seq{for i in 1 ..n do 
                    yield n}
let alter = Seq.unfold (fun x -> Some(x, not x)) true
let bins = init0 10000000 |> Seq.zip alter |> Seq.collect (fun (x,y)->Rep y x) 



