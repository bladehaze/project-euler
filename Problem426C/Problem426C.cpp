// Problem426C.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

void MoveOneWithNoIterator(long* data,int len)
{
    bool curentisball = true;
    long unassignedballs = 0;
    long preemptybox = 0;
    for (int i = 0; i < len; i++)
    {
        long curnum = data[i];
        if(i==0)
        {
            curentisball = false;
            unassignedballs = curnum;
            preemptybox = 0;
            continue;
        }
                
        if(!curentisball)
        {
            if(curnum<unassignedballs)
            {
                data[i - 1] = curnum;
                unassignedballs -= curnum;
                curentisball = true;
                preemptybox = 0;
                continue;
            }else
            {
                data[i - 1] = unassignedballs;
                preemptybox = curnum - unassignedballs;
                unassignedballs = 0;
                curentisball = true;
            }
        }else
        {
            unassignedballs += curnum;
            curentisball = false;
            data[i - 1] = curnum + preemptybox;
            preemptybox = 0;
        }
    }
    data[len - 1] = unassignedballs;
}
long* Init(int n){
	long*p = new long[n];
	long  long s0 = 290797;
	long  long md = 50515093;
	for(int i=0;i<n;++i){
		if(i==0)
		{
			p[i] = (s0%64)+1;
			continue;
		}
		long  long z = s0*s0;
		s0 = (z)%md;
		p[i] = (s0%64)+1;
	}
	return p;
}

int _ztmain(int argc, _TCHAR* argv[])
{
	long* p ;
	p = Init(10000001);
	//for(int i = 0 ; i<11 ; i++)
	//{
	//	std::cout << p[i]<<std::endl;
	//}
	//std::cout << std::endl;
	for(int i=0;i<10000;i++)
	{
		MoveOneWithNoIterator(p,10000001);
		std::cout << i << std::endl;
	}

	
	for(int i = 0 ; i<11 ; i++)
	{
		std::cout << p[i]<<std::endl;
	}
	return 0;
}

