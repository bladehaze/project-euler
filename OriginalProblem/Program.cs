﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OriginalProblem
{
    class Program
    {
        static void Main(string[] args)
        {
            var zz = NumOfPath(15,3);
        }

        static long[,] NumOfPath(int T, int allowmove)
        {
            long TotalMatrix = 2*T*allowmove + 1;
            int nn = T*allowmove;
            var ma = new long[T,TotalMatrix];
            ma[0, nn] = 1;
            for (int i = 1 ; i <T; i++)
            {
                for (int j = -allowmove * i ; j <= allowmove * i; j++)
                {
                    var res = Enumerable.Range(-allowmove, 2*allowmove+1).Sum(e => ma[i - 1, e+nn+j]);
                    ma[i, j + nn] = res;
                }
            }
            return ma;
        }
    }
}
