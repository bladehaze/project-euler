﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinqExtension
{
    public static class LinqExtension
    {
        public static IEnumerable<T> AppendRange<T>(this T x, IEnumerable<T> items )
        {
            yield return x;
            foreach (var item in items)
            {
                yield return item;
            }
        }

        public static IEnumerable<T> Prepend<T>(this IEnumerable<T> data, T v )
        {
            yield return v;
            foreach (var VARIABLE in data)
            {
                yield return VARIABLE;
            }
        }
        public static IEnumerable<T> EveryOther<T>(this IEnumerable<T> data )
        {
            bool z = true;
            foreach (var VARIABLE in data)
            {
                if(z)
                {
                    yield return VARIABLE;
                }
                z = !z;
            }
        }
        public static IEnumerable<V> ActionOnMovingWindow<T,V>(this IEnumerable<T> data, Func<IEnumerable<T>,V> func ,int lengthofwindow )
        {
            Queue<T> tmp = new Queue<T>();
            foreach (var VARIABLE in data)
            {
                tmp.Enqueue(VARIABLE);
                if (tmp.Count == lengthofwindow)
                {
                    yield return func(tmp);
                    tmp.Dequeue();
                }
                
            }
        }
        public static IEnumerable<T> CumSum<T>(this IEnumerable<T> data, Func<T,T,T> func  )
        {
            bool isfirst = true;
            T tmp = default (T);
            foreach (var VARIABLE in data)
            {
                if(isfirst)
                {
                    tmp = VARIABLE;
                    isfirst = false;
                }
                tmp = func(tmp, VARIABLE);
                yield return tmp;
            }
        }

        public static int Product(this IEnumerable<int> data )
        {
            return data.Aggregate((cur, pro) => cur*pro);
        }
    }
}
