﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LinqExtension
{
    public static  class ReadFile
    {
        public static IEnumerable<string> ReadLine(string filename)
        {
            using (var file = new StreamReader(filename))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    yield return line;
                }
            }
        }
        public static T[,] ReadMatrix<T>(string filename,char delimiter, Func<string,T> func )
        {
            var l = ReadLine(filename).Select(g => g.Split(delimiter).ToList()).ToList();
            var col = l.Max(e => e.Count);
            var res = new T[l.Count,col];
            int i = 0;
            foreach (var VARIABLE in l)
            {
                var ss = VARIABLE;
                int j = 0;
                foreach (var v in ss)
                {
                    res[i, j] = func(v);
                    j++;
                }
                i++;
            }
            return res;
        }
    }
}
